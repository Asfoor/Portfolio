//ExpressionEvaluator.java Class #2 Evaluate Button


/**
 * Working class, this is what goes on when we hit evaluate after filling in blank 
 * with an expression. Does our evaluation of the equation/expression
 * @author Anes Mehai
 *
 */
public class ExpressionEvaluator {

	/**
	 * Evaluate the expression. This is 
	 * what happens when evaluate button is pressed behind the scene
	 * @param infix
	 * @return
	 */
	public static int evaluate(Expression infix) {

		/* Infix --> Postfix Conversion 
		 * institiante new objecy to use convert method from class created
		 */InfixToPostfixConvert conversion = new InfixToPostfixConvert(infix);

		 // Convert infix expression to postfix expression
		 conversion.convertToPostfix();

		 Expression postfix = conversion.getPostfix();

		 // Print postfix testing purposes
		 System.out.println("Postfix Expression : " + postfix.toString());

		 // Instantiate a PostfixEvaluator and now can evaluate what user put from GUI
		 PostfixEvaluator postvalue = new PostfixEvaluator(postfix);

		 // Evaluate the postfix expression
		 int answer = postvalue.evaluate();

		 // lastly return calculated value (result)
		 return answer;
	}
}
