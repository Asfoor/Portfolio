import java.util.ArrayList;

/**
 * One of the working classes, this takes infix expression user from GUI and converts it to Postfix
 * @author Anes Mehai
 *
 */
public class InfixToPostfixConvert {


	// Declare infix-postfix expression
	private Expression infixExpression;
	private Expression postfixExpression;


	/**
	 * Constructor
	 * @param infix
	 */
	public InfixToPostfixConvert(Expression infix) {

		infixExpression = infix;
	}

	/**
	 * convert to postfox expression
	 */
	public void convertToPostfix() {
		/* Create empty expression postfix, with empty operator stack, 
		 * initialize tokens and use pseudocode to figure out loop and convert expression
		 * to give answer
		 */


		//Instantiation postfix Expression & empty operator stack
		postfixExpression = new Expression();

		ArrayListStack<Token> operatorStack = new ArrayListStack<Token>();

		// Temp local variables
		Token topOfStack = null;
		Token nextToken = null;

		//ArrayList of Tokens from expression input. Put in order then will be making 
		//loop to fill up our expression so we can take it and calculate it
		ArrayList<Token> ogExpression = infixExpression.getExpression();

		
		//iterate through our Arraylist
		for (int mainCounter = 0; mainCounter < ogExpression.size(); mainCounter++)
		{

			// Get next token
			nextToken = ogExpression.get(mainCounter);

			/*
			 * Here come the condition and precedents and if
			 */

			// If it is an operand push it onto operand stack aka postfix expression
			if (nextToken.isOperand())
				postfixExpression.add(nextToken);

			// If it is ( open parenthesis push into stack
			else if (nextToken.isOpenParenthesis())
				operatorStack.push(nextToken);

			// Same for ) closed parenthesis
			else if (nextToken.isClosedParenthesis())
			{
				//next part while loop, while its not ( do the following from pseudocode
				// this means when top = (

				//make topofstack token = top
				topOfStack = operatorStack.top();

				/*
				 * 1 pop 2 operands & operator
				 * 2 perform calculations
				 * 3 push results on stack
				 */
				while(!topOfStack.isOpenParenthesis())
				{
					postfixExpression.add(topOfStack);
					operatorStack.pop();
					topOfStack = operatorStack.top();

				}

				//remember to discard the left parenthesis
				operatorStack.pop();
			}

			//continue with pseudocode, will be comparing precedences 
			//if it is an operator
			else if (nextToken.isOperator()) {
				//get precedence of token
				int precedenceOfToken = nextToken.getOrderPrecedence();


				//make a check operator stack is not empty condition

				if(operatorStack.isEmpty())
					operatorStack.push(nextToken);

				else	{

					//get precedence of the top of the stack
					topOfStack = operatorStack.top();

					//
					if(topOfStack.isOpenParenthesis())
						operatorStack.push(nextToken);

					else	{
						//
						int precedenceOfStack = topOfStack.getOrderPrecedence();

						//
						//

						if(precedenceOfToken > precedenceOfStack)
							operatorStack.push(nextToken);

						else	{

							//
							//
							//

							while ((precedenceOfToken <= precedenceOfStack) 
									&& (!operatorStack.isEmpty()) && (!topOfStack.isOpenParenthesis())) {


								topOfStack = operatorStack.pop();
								postfixExpression.add(topOfStack);

								if(!operatorStack.isEmpty()) {
									topOfStack = operatorStack.top();

									precedenceOfStack = topOfStack.getOrderPrecedence();
								}

							}


							// At the end push nextToken into Stack
							operatorStack.push(nextToken);


						}
					}

				}

			}


			else	{
				System.out.println("Error home boy, String not recognize in InfixToPostfixConversion");
				System.out.println(ExpressionEvaluator.evaluate(postfixExpression));
				break;

			}

		}


		// Continuing from pseocode 

		while (!operatorStack.isEmpty())	
		{
			topOfStack = operatorStack.pop();

			postfixExpression.add(topOfStack);

		}

	}

	/**
	 * Returns postfix expression when called
	 * @return postfix expression
	 */
	public Expression getPostfix() {
		return postfixExpression;
	}



}
