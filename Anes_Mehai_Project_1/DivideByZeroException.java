
public class DivideByZeroException extends ArithmeticException {

	String message = "Error fix entry and try again";

	public DivideByZeroException( String message )
	{
		this.message = message;
	}

	public String toString()
	{
		return( message );
	}

}
