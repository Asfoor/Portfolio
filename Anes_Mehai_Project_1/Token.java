/**
 * Define the tokens and what they are in the order of operation PEMDAS
 * @author Anes Mehai
 *
 */
public class Token {

	// Parenthesis char
	private final static char openParenthesis = '(';
	private final static char closedParenthesis = ')';

	// Identifies this as OPERATORS, static to be seen elsewhere?
	//Get javadoc for this data type and methods and others after it
	private final static String[] recognizeOperators = 
		{ "+", "-", "*", "/", "%", "<", "<=", ">", ">=", "==", "!=", "||", "&&" };


	// Holds whats inside the token
	private String tokenBody;

	//Constructor
	public Token (String tokenBody)
	{
		this.tokenBody = tokenBody;
	}

	/**
	 * 
	 * @return
	 */
	public String getTokenBody(){
		return tokenBody;
	}

	/**
	 * Check if token is operator
	 * @return
	 */
	public boolean isOperator(){

		for (int i = 0; i < recognizeOperators.length; i++)
		{
			if (recognizeOperators[i].equals(tokenBody))
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Check if token is (
	 * @return true or false
	 */
	public boolean isOpenParenthesis(){

		char value = tokenBody.charAt(0);
		return (value == openParenthesis);

	}

	/**
	 * Check if token is )
	 * @return true or false
	 */
	public boolean isClosedParenthesis(){

		char value = tokenBody.charAt(0);
		return (value == closedParenthesis);

	}

	/**
	 * Check if token is operand 
	 * @return
	 */
	public boolean isOperand(){

		return (!(( isOperator() || isOpenParenthesis() || isClosedParenthesis() )));
	}

	//make javadoc
	/**
	 * 
	 * @return
	 */
	public int getOrderPrecedence(){

		if (tokenBody.equals("<") || tokenBody.equals("<=") || 
				tokenBody.equals(">") || tokenBody.equals(">="))
			return 1;

		else if (tokenBody.equals("==") || tokenBody.equals("!="))
			return 2;

		else if (tokenBody.equals("||"))
			return 3;

		else if (tokenBody.equals("&&"))
			return 4;

		else if (tokenBody.equals("+") || tokenBody.equals("-"))
			return 5;

		else if (tokenBody.equals("*") || tokenBody.equals("/") || tokenBody.equals("%"))
			return 6;
		return -1;

	}

	public String toString(){

		return tokenBody;
	}


}
