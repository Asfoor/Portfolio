//InfixExpressionEvaluatorGUI.java add javadoc

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class InfixExpressionEvaluatorGUI extends JFrame {




	//working class
	private ExpressionEvaluator expressionEvaluator;

	// The panels and the sub panels to make the correct layout
	private JPanel mainPanel, panelOne, panelTwo, panelThree;

	//Declare Labels
	private JLabel expressionLabel;
	private JLabel expressionResultLabel;

	//Declare Textfields
	private JTextField expressionField;
	private JTextField expressionResultField;

	// Declare Buttons
	private JButton evaluateButton;
	private JButton exitButton;


	//make Java Doc
	//Constructer pass in working class object ExpressionEvaluator to evaluate the expression
	//This is where we build on GUI, and set size, buttons etc
	public InfixExpressionEvaluatorGUI(ExpressionEvaluator ee1) {

		// Set expression evaluator
		this.expressionEvaluator = expressionEvaluator;

		instantiate();
		buildGUI();
		add(mainPanel);
		addListeners();


		// Set Title
		setTitle("Infix Expression Evaluator");

		// Set default jframe close operation
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Set Size
		setSize(400,150);

		// Lock Size of window
		setResizable(false);

		// Set window visible
		setVisible(true);

		pack();






	}



	/**
	 * Instantiates the GUI components to match the assignments description
	 */
	private void instantiate() {

		// Initialize GUI elements

		// Initialize Panels
		mainPanel = new JPanel(); 
		panelOne = new JPanel(); 
		panelTwo = new JPanel();
		panelThree = new JPanel();


		// Initialize Labels
		expressionLabel = new JLabel("Enter Infix Expression");
		expressionResultLabel = new JLabel("Result");


		// Initialize Fields
		expressionField = new JTextField(18);
		expressionResultField = new JTextField(12);

		// Set display fields to be not editable for result
		expressionResultField.setEditable(false);


		// Initialize Buttons
		evaluateButton = new JButton("Evaluate");
		exitButton = new JButton("Exit");


	}



	/**
	 * Builds the GUI by adding the components to the frame.
	 */
	private void buildGUI() {



		// Set Layout
		mainPanel.setLayout(new GridLayout(3, 1));
		panelOne.setLayout(new FlowLayout());
		panelTwo.setLayout(new FlowLayout());
		panelThree.setLayout(new FlowLayout());


		// Add Fields-Labels-Buttons to sub Panels
		panelOne.add(expressionLabel); 
		panelOne.add(expressionField);
		panelTwo.add(evaluateButton);
		panelThree.add(expressionResultLabel);
		panelThree.add(expressionResultField);
		panelThree.add(exitButton);

		// Add subPanels to main content panel
		mainPanel.add(panelOne);
		mainPanel.add(panelTwo);
		mainPanel.add(panelThree);





	}

	/**
	 * Adds listeners to the GUI buttons, evaluate and quit
	 */
	private void addListeners() {

		// Add listener to button
		evaluateButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {



				// Set Error message
				String expressionValueString = "Error, try again";

				// Get expression
				Expression infixExpression = new Expression(expressionField.getText());

				// Evaluate Expression and save it
				int expressionValue = expressionEvaluator.evaluate(infixExpression);

				// forgot what had to be fixed here go back and Check if something went wrong, ex: Divisible by 0
				if (expressionValue != Integer.MIN_VALUE) 
				{
					expressionValueString = Integer.toString(expressionValue);
				}

				//divideby0exception

				PostfixEvaluator pe1 = new PostfixEvaluator(infixExpression);
				
				



				// Set expression value field
				expressionResultField.setText(expressionValueString);

			}

		});

		// Add listener to exit button
		exitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
	}

	public static void main(String[] args) {

		// Instantiate an ExpressionEvaluator working class to be used by the GUI
		ExpressionEvaluator ee1 = new ExpressionEvaluator();

		// Create instance of expression evaluator GUI
		new InfixExpressionEvaluatorGUI(ee1);
	}







}
