import javax.security.auth.callback.TextOutputCallback;

/**
 * Our ArrayListStack must implement these important abstract methods
 * @author puttinwork
 *
 * @param <T>
 */
public interface Stack<T> {

	// list methods that needs to be implemented

	/**
	 * Check if stack is empty or not
	 * @return boolean true or false
	 */
	public boolean isEmpty();

	/**
	 * Add new element to stack from bottom going up
	 * @param item
	 */
	public void push(T item);

	/**
	 * Remove element from the stack starting from the top until reach 
	 * destination
	 * @return
	 */
	public T pop();

	/**
	 * All the way up, what is the top element
	 * @return
	 */
	public T top();







}
