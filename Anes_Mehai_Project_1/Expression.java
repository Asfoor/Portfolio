//Expression.java make java doc

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * This is where we store our expression. From the GUI, we take our user input
 * and save the JTextField into an Expression which will be held in arrayListStack 
 * We can iterate through our stack in order to find precedents and expression evaluation
 * (infix, infixtopostfix, postfix and final answer /result convert from infix to postfix.
 * 
 * Use this class to manipulate, delete and store user input (infixexpression)
 * @author Anes Mehai
 *
 */
public class Expression {

	// Declare arrayList
	private ArrayList<Token> expression;

	/**
	 * Default Constructor
	 */
	public Expression() {
		// Create the array that represents the body of the Expression
		expression = new ArrayList<Token>();
	}

	/**
	 * Constructor that will take in a String of user entered expression and will read 
	 * entire entry until there is no more elements. 
	 * @param expressionInput
	 */
	public Expression(String expressionInput) 
	//* This can only be done with no space in between could not figure out how to include white space as delim..

	{
		// Instantiate the arrayList named expression that will hold tokens
		expression = new ArrayList<Token>();

		//not sure if in project you want us to include white spaces. Mine programs only works when there are no spaces
		StringTokenizer stringToken = 
				new StringTokenizer(expressionInput, "( , ) , + , - , *, /, %, <, <=, >, >=, ==, !=, ||, &&, ,	,",  true);


		// Scan the input
		while (stringToken.hasMoreElements()) {
			Token newTok = new Token((String) stringToken.nextElement());
			expression.add(newTok);
		}
	}

	/**
	 * Add new data to stack
	 * @param newElement
	 */
	public void add(Token newElement) {
		expression.add(newElement);
	}


	/**
	 * Access expressions from our ArrayListStack using Tokens
	 * @return expression from the ArrayList
	 */
	public ArrayList<Token> getExpression() {
		return expression;
	}

	/**
	 * Length/size of the expression array
	 * @return expression.size
	 */
	public int size() {
		return expression.size();
	}

	/**
	 * Convert our expression (Token) to a String and return its value as a String
	 */
	public String toString() {
		String returnValue = "";
		for (int i = 0; i < expression.size(); i++)
			returnValue = returnValue + expression.get(i).getTokenBody() + " ";
		return returnValue;
	}
}