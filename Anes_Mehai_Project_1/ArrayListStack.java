import java.util.ArrayList;

/**
 * This is where we specify that we want our ArrayList to be treated as a Stack with 
 * specifications, implement
 * 
 * @author Anes Mehai
 *
 * @param <T>
 */
public class ArrayListStack<T> implements Stack<T> {


	// Declare Stack 
	private ArrayList<T> stackBody;


	// Initialize Stack 
	public ArrayListStack() {
		stackBody = new ArrayList<T>();
	}

	// Define methods that need to be implemented


	/**
	 * Return if empty or not
	 */
	public boolean isEmpty() {
		return (stackBody.size() == 0);
	}


	/**
	 * Adds element to stack starting from the bottom up. FILO
	 */
	public void push(T item) {
		stackBody.add(item);
	}

	/**
	 * Deletes element from stack, in order to do this must start from the top 
	 * and work our way to the element we would like to delete
	 */
	public T pop() {
		if (isEmpty()) 
		{
			System.out.println("Error in ArrayStack.pop() Stack Is Empty ");

			return null;
		} 

		else {

			T firstElement = stackBody.get(stackBody.size() - 1);

			//update length of arrayList stack
			stackBody.remove(stackBody.size() - 1);
			return firstElement;
		}
	}

	/**
	 * Keeps track of the first/top element, checks if empty first before giving correct answer
	 */
	public T top() {
		if (isEmpty()) 
		{
			System.out.println("Error in ArrayStack.top() Stack Is Empty ");

			return null;
		} 

		else {

			T firstElement = stackBody.get(stackBody.size() - 1);
			return firstElement;
		}
	}
}

