import java.util.ArrayList;

/**
 * This is out other working class that deals with evaluating postfix
 * @author Anes Mehai
 *
 */
public class PostfixEvaluator {

	// Declare postfix expression, value of expression
	private Expression postfixExpression;

	private int numValueOfExpression;
	
	private boolean flagException = false;

	/**
	 * @return the flagException
	 */
	public boolean isFlagException() {
		return flagException;
	}

	/**
	 * @param flagException the flagException to set
	 */
	public void setFlagException(boolean flagException) {
		this.flagException = flagException;
	}

	/**
	 * Constructor:
	 * 	Constructs evaluator from postflix expression
	 * 
	 */
	public PostfixEvaluator(Expression postfix){
		postfixExpression = postfix;

	}

	/**
	 * Evaluates postfix expression our last step to our evaluate button
	 * @return value
	 */
	public int evaluate() {

		/*
		 * Loop through the expression and evaluate using proper PEMDAS
		 * Start with empty operand stack
		 */

		ArrayListStack<Token> operandStack = new ArrayListStack<Token>();

		//temp variables to keep track of next token and where I'm at
		Token nextToken;

		ArrayList<Token> postfix = postfixExpression.getExpression();


		//main loop we iterate and pulls values from
		for(int mainLoop = 0; mainLoop < postfix.size(); mainLoop++) {

			//CCCCC Get the next token from postfix expression
			nextToken = postfix.get(mainLoop);

			// If it is operand, push into stack
			if(nextToken.isOperand()) {

				operandStack.push(nextToken);
				// Testing System.out.print(operandStack + "Hi");
			}

			//if it is an operator
			else if (nextToken.isOperator())	{

				// Get 2 operands out of stack
				if(operandStack.isEmpty()) {
					System.out.println("Error in PostfixEvaluator.evaluate() " 
							+ "-- Incorrect input expression");

					return Integer.MIN_VALUE;
				}

				Token operandTwo = operandStack.pop();

				if(operandStack.isEmpty()) {
					System.out.println("Error in PostfixEvaluator.evaluate() " 
							+ "-- Incorrect input expression");

					return Integer.MIN_VALUE;

				}

				Token operandOne = operandStack.pop();

				/*
				 * We are finally able to evaluate the input
				 */

				Token result = calculate(nextToken, operandOne, operandTwo);

				// Now that we have the result we push that into the stack
				operandStack.push(result);

				

			}

		}

		// When only 1 element left in stack
		if(operandStack.isEmpty()) {

			System.out.println("Error in PostfixEvaluator.evaluate() " 
					+ "-- Incorrect input expression");

			return Integer.MIN_VALUE;

		}

		// Now final stretch, we take operand out of the stack and convert it to result integer
		Token topToken = operandStack.pop();

		numValueOfExpression = Integer.parseInt(topToken.getTokenBody());

		if (operandStack.isEmpty())
			return numValueOfExpression;

		else	{

			System.out.println("Error in PostfixEvaluator.evaluate() " 
					+ "-- Incorrect input expression");

			return Integer.MIN_VALUE;
		}

	}

	/**
	 * 
	 * @param operatorToken
	 * @param operandOne
	 * @param operandTwo
	 * @return
	 */
	private Token calculate(Token operatorToken, Token operandOneToken, Token operandTwoToken) {

		// Get operator from token
		String operator = operatorToken.getTokenBody();

		// Convert operand 1&2 from String to int
		int operandOne = Integer.parseInt(operandOneToken.getTokenBody());
		int operandTwo = Integer.parseInt(operandTwoToken.getTokenBody());

		//Set return value to highest value
		int result = Integer.MAX_VALUE;

		/*
		 * Perform expression operation according to PEMDAS and give 
		 * result a value to be returned at the end
		 */

		if(operator.equals("<")) {

			if (operandOne < operandTwo)
				result = 1;
			else
				result = 0;
		}	else if (operator.equals("<="))		{

			if (operandOne <= operandTwo)
				result = 1;
			else
				result = 0;
		}	else if (operator.equals(">"))	{

			if (operandOne > operandTwo)
				result = 1;
			else
				result = 0;
		}	else if (operator.equals(">="))	{

			if (operandOne >= operandTwo)
				result = 1;
			else
				result = 0;
		}	else if (operator.equals("=="))		{

			if (operandOne == operandTwo)
				result = 1;
			else
				result = 0;
		}	else if (operator.equals("!="))		{

			if (operandOne != operandTwo)
				result = 1;
			else
				result = 0;
		}	else if (operator.equals("||"))		{

			if (operandOne !=0 || operandTwo != 0)
				result = 1;
			else
				result = 0;
		}	else if (operator.equals("&&"))		{

			if (operandOne != 0 && operandTwo !=0)
				result = 1;
			else
				result = 0;

		}	else if (operator.equals("+"))		{

			result = operandOne + operandTwo;
		}	else if (operator.equals("-"))		{

			result = operandOne - operandTwo;
		}	else if (operator.equals("*"))		{

			result = operandOne * operandTwo;
		}	else if (operator.equals("/"))		{

			//check divisibility by 0 and throw exception
			try {
				if (operandTwo != 0)
					result = operandOne/operandTwo;
				else
					setFlagException(true);
					System.out.println("Error! Cannot divide by 0" + " PostfixEvaluator.calculate().");
			} catch (ArithmeticException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}	else if (operator.equals("%"))		{

			if (operandTwo != 0)
				result = operandOne % operandTwo;
			else{
				
				System.out.println("Error! Cannot divide by 0" + " PostfixEvaluator.calculate().");
			}
		}	else	{
			System.out.println("Unrecognized operator in " + "PostfixEvaluator.calculate()");	
		}

		
		// Lastly convert result into a token and return it

		return new Token ("" + result);
	}





}
