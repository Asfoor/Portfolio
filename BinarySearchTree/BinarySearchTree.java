package BinarySearchTree;

/**
 * 
 * 		BinarySearchTree<T> is a Generic class, this means after the class 
 * name adding <> with inside type parameters T, T1... 
 * 
 * In simplest terms
 * this means instead of being Type specific, the class name and its methods, constructors
 * are broad/generic, instead of return or requiring an object or data type
 * it takes <Type Variable>. The parameters takes any type variables <T> and can perform
 * all the methods in the Generic class with various data types. For this project
 * having a Generic BST allows me to make a BST for various data types, for this project
 * they are
 * - Integer BST
 * - Fraction BST
 * 
 * I can generate a Tree and output the data in ascending and descending order in
 * <T>, for this project we defined it only for Integers and Fractions.
 * 
 * -Generic means that more than one type may use
 * 
 * Java Documentation From Oracle on Generic Type:
 * Java Generic methods and generic classes enable programmers to specify, 
 * with a single method declaration, a set of related methods, 
 * or with a single class declaration, a set of related types, respectively.
 * 
 * 		This is the class where I build my data structure, a Binary Search Tree.
 * A BST is A data struc­ture in which we have nodes that con­tain­
 * - data and 
 * - 2 ref­er­ences to other nodes, one on the left and one on the right.
 * A BST has the following properties:
 * - The parent node = root and,
 * - The left sub-tree of a node has a key less than or equal to its 
 * parent node's key.
 * - The right sub-tree of a node has a key greater than to its 
 * parent node's key.
 * 
 * Nodes are objects of a class and each node has:
 *  - data and a link to the left node and right node.
 *  - Usu­ally we call the start­ing node of a tree as root.
 *  - Nodes smaller than root goes to the left of the root and 
 *  - Nodes greater than root goes to the right of the root.
 * 
 * 
 * @author Anes Mehai
 *
 * @param <T> Generic Type, Fraction or Integer
 */
public class BinarySearchTree<T> {


	/*
	 * What to do:
	 * 
	 * The user input list = OG list, are to be added to a binary search tree.
	 * 
	 * To do this you must first iterate through the whole list and add it to 
	 * and array one by one.
	 * 
	 * Once you have the original list in an array, generate a binary search tree.
	 * 
	 *  Then an inorder traversal of the tree should be performed to generate the list
	 *  in sorted order and that list should then be displayed in the sorted list text field.
	 */


	T data; // data of current node

	BinarySearchTree parent; //root
	BinarySearchTree leftside; //reference to left node
	BinarySearchTree rightside; //reference to right node

	String textlist; // String representation of sorted list (accessible by root.data)

	/**
	 * Default Constructor
	 */
	public BinarySearchTree() {


		//basic Init

		data = null;

		parent = null;

		leftside = null;
		rightside = null;
	}

	/**
	 * Constructor that takes in Data and init
	 * left and right node to Null. If left and ride nodes
	 * are Null, it signifies the end of the Tree.
	 * @param data, Data of the list
	 */
	public BinarySearchTree(T data){

		this.data = data;
		leftside = null;
		rightside = null;
	}

	/**
	 * Create a new Node to be added to the Tree
	 * @param data, Generic data to be entered into the tree.
	 * @return newBSTNode, new node with all of its properties.
	 */
	BinarySearchTree<T> createBinarySearchTreeNode(T data) {

		BinarySearchTree<T> newBSTNode = new BinarySearchTree<T>();

		newBSTNode.data = data;

		newBSTNode.parent = null;

		newBSTNode.leftside = null;
		newBSTNode.rightside = null;


		return newBSTNode;
	}

	/**
	 * Insert the new node in the appropriate place on the Tree.
	 * @param tree, Tree of our data
	 * @param node, Our current node we wish to enter
	 * @return tree, return new tree with the new node inserted
	 */
	BinarySearchTree<T> insertBinarySearchTree(BinarySearchTree<T> tree, BinarySearchTree<T> node) {

		BinarySearchTree<T> previous = null;
		BinarySearchTree<T> iterate = tree;


		//iterate the old tree to the new tree with the newly added node.


		//recursion, will keep going until iterated tree is empty
		while (iterate != null) 
		{

			previous = iterate;

			// If its a Integer go here and compare data, get the reference for right and left
			if (tree.data instanceof Integer) 
			{
				if ((Integer) node.data < (Integer) iterate.data)
				{
					iterate = iterate.leftside;
				}

				else
				{
					iterate = iterate.rightside;
				}
			}	

			else	{

				// If its a fraction go here and compare data, get the reference for right and left
				if(((Fraction)node.data).compareTo(((Fraction)iterate.data)) >0)
				{
					iterate = iterate.leftside;
				}

				else
				{
					iterate = iterate.rightside;
				}
			}



		}

		// found node is parent to our node
		node.parent = previous;

		/*if previous is NULL
		// then this is the first node
		 change the root(current) NODE
		 */
		if (previous == null)
		{
			tree = node;
		}

		else 

		{
			// Here decide where the node should go: left or right
			if (tree.data instanceof Integer) 
			{
				if ((Integer) node.data < (Integer) previous.data)
				{
					previous.leftside = node;
				}
				else
				{
					previous.rightside = node;
				}
			}

			else

			{
				if(((Fraction)node.data).compareTo(((Fraction)previous.data)) > 0)
				{
					previous.leftside = node;
				}

				else
				{
					previous.rightside = node;
				}

			}

		}

		return tree;
	}

	/**
	 * Convert the array that was iterated and filled accordingly, 
	 * to a Binary Search Tree. Takes in the array with ordered list, and its size
	 * @param array[], list in an array representation
	 * @param size, size/length of the array in integer
	 * @return tree, The tree 
	 */
	BinarySearchTree<T> convertArrayToBST(T array[], int size) {

		BinarySearchTree<T> tree = null;

		//iterate through my list array and form a tree one by one.
		for (int i = 0; i < size; i++) 
		{
			tree = insertBinarySearchTree(tree, createBinarySearchTreeNode(array[i]));
		}
		return tree;
	}

	/**
	 * 	String representation of the list in ascending order
	 * 
	 * @param root, The first element, the parent node in the Tree.
	 * @return textlist, String representation of list in ascending order
	 */
	String ascending(BinarySearchTree root) {

		//If current position is empty
		if (root == null)
		{
			return textlist;
		}

		//--> LEFT TO RIGHT ascending
		ascending(root.leftside);
		
		textlist += String.valueOf(root.data + " ");
		
		ascending(root.rightside);
		
		return textlist;
	}

	/**
	 * 	String representation of the list in descending order
	 * 
	 * @param root, The first element, the parent node in the Tree.
	 * @return textlist, String representation of list in descending order
	 */
	String descending(BinarySearchTree root) {

		if (root == null)
		{
			return textlist;
		}
		
		// RIGHT TO LEFT <-- descending
		descending(root.rightside);
		
		
		
		textlist += String.valueOf(root.data + " ");
		
		descending(root.leftside);
		
		return textlist;
	}


}

