/**
 * Run the main Method in BinarySearchTreeSortGUI
 * 
 * Worker class BinarySearchTree
 * 
 * Fraction class handles putting (comparing) fraction in 
 * ascending and descending order
 */
/**
 * @author Anes Mehai
 *
 */
package BinarySearchTree;