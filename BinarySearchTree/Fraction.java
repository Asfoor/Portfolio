package BinarySearchTree;

/**
 * 		Fraction is a class that implements the Comparable interface
 * 
 * 		Comparable is used to order the objects of user-defined class.
 * In order to implement the interface I must define only one method named 
 * compareTo(Object). 
 * It provide single sorting sequence only i.e. you can sort the elements on based on single data member only. 
 *
 *		Having the comparable interface allows me to compare fractionValue with other
 * fractionValue. 
 * It will be used to put user-inputed fraction list in ascending or descending order
 * after pressing the perform sort button.
 *	
 *		This class is used only when user selects numeric type "Fraction" in the GUI 
 *
 *		Private data is the String representation of a fraction.
 *
 *		There is a default constructor and a constructor that accepts a String
 *representation of a fraction
 * 
 * 		There are a few methods, setters and getters, compareTo method 
 * and a toString method.
 * 
 * 		Method names: getValueOfFractionValue(), 
 * setValueOfOfFractionValue(String fractionValue), 
 * toString() and compareTo(Object node).
 * 
 * @author Anes Mehai
 *
 */
public class Fraction implements Comparable {

	private String fractionValue;

	/**
	 * Default Constructor
	 */
	Fraction(){
		//basic init

		super();

		this.fractionValue = null;
	}

	/**
	 * Constructor that accepts a String representation of a fraction
	 * @param fractionValue - String representation of our Fractional value
	 */
	Fraction(String fractionValue){

		setValueOfOfFractionValue(fractionValue);
	}

	/**
	 * Getter, get the String value of fractionValue
	 * @return fractionValue, the value of the fraction in String representation
	 */
	public String getValueOfFractionValue() {

		return fractionValue;
	}

	/**
	 * Setter, set the value of fractionValue. This is where GUI takes 
	 * in user-input data and gives fractionValue what it equals
	 * @param fractionValue, the value of the fraction in String representation
	 */
	public void setValueOfOfFractionValue(String fractionValue) {

		this.fractionValue = fractionValue;
	}

	
	/**
	 * Have to implement this method in order to 
	 * implement comparable interface.
	 * 
	 * It provide single sorting sequence only i.e. you can 
	 * sort the elements on based on single data member only.
	 */
	public int compareTo(Object node) {

		//The return value
		int returnValue;

		//So I can compare string to string
		String intToString = node.toString();

		//Have an array of Strings, "/" signifying a fracion
		String valuesFromList[] = intToString.split("/");

		/*
		 * Comparing the 2 fractions in question
		 */
		
		
		
		//first
		Float first = Float.valueOf(valuesFromList[0]) /
				Float.valueOf(valuesFromList[1]);
		
		
		//second compare fraction
		valuesFromList = fractionValue.split("/");

		// result
		Float result = Float.valueOf(valuesFromList[0]) / 
				Float.valueOf(valuesFromList[1]);

		// comparing our first fractionValue with the next element being compared
		// make it equal to the value that I will be returning
		returnValue = first.compareTo(result);

		return returnValue;
	}

	/**
	 * toString Method, Converting data to a String representation of fractionValue
	 */
	public String toString() {

		String message = getValueOfFractionValue();

		return message;
	}

}
