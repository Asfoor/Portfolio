package BinarySearchTree;


/**
 * 		NumberFormatExpression is a class that extends the 
 * parent class NumberFormatException.
 * 
 * 		This class will be used to verify user-input data is valid 
 * ex: To check for nonnumeric input which includes imroperly formatted
 * fractions such as 3/4/8. This class/exception will be caught and run in the
 * main class and display an appropriate error message in the JPanelOption
 * 
 * 		Thrown to indicate that the application has attempted to convert 
 * a string to one of the numeric types, 
 * but that the string does not have the appropriate format.
 * @author Anes Mehai
 *
 */
public class NumberFormatExpression extends NumberFormatException {

	private String errorMessage;
	
	/**
	 * Default Constructor
	 * 		When NumberFormatExpression is called it will 
	 * call setErrorMessage method with the appropriate error message
	 */
	public NumberFormatExpression(){

		setErrorMessage("Malformed Fraction Fam, Try Again");
	}
	
	
	/**
	 * Getter, get the ErrorMessage for the NumberFormatException
	 * @return errorMessage, "Non Numeric Input"
	 */
	public String getErrorMessage() {

		return errorMessage;
	}

	
	/**
	 * Setter, set what the Error Message should contain
	 * @param errorMessage, "Non Numeric Input"
	 */
	public void setErrorMessage(String errorMessage) {

		this.errorMessage = errorMessage;
	}



}
