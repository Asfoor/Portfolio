package BinarySearchTree;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.border.TitledBorder;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;


/**
 This class is our GUI, it is where our main is and where we run our program.
 * This class extends JFrame and uses Swing to provide our GUI. Every instance variable
 * must be declared as private.
 * 
 * This class works by having the constructor call 4 methods:
 * 
 *  - instantiate my variables, buttons, labels, Jradiobuttons, Borders, etc so I can use them
 *  - buildGui: builds my gui with the layout and look I want
 *  - and adding my mainPanel made of subPanels 
 *  - and Listeners for my buttons.
 * 
 * 
 * 		Running this program will bring up a GUI where the user will a list of numbers
 * in integer or fraction form. Integer are seoerated by one space and fraction are 
 * no space between the slash and space before and after the number. 
 *  Pressing the Perform Sort will require the user to state whether it wants the data
 *  ordered in ascending or descending and whether the data shown is that of integer or fraction.
 * 
 * To run our program we must create our ThreeAddressGeneratorGUI and pass in our
 * TreeExpressionEvaluator to build our Tree, and do the work to convert the postfix
 * expression to infix expression. 
 * 
 * Project 2:
 * 
 * - A program that accepts an arithmetic expression of unsigned integers in postfix 
 * notation
 * - And builds the arithmetic expression tree that represents that expression.
 * 
 * - From that tree, the corresponding fully parenthesized infix expression 
 * should be displayed
 * - And a file should be generated that contains the 3 address format instructions. 
 *
 *
 *
 */
public class BinarySearchTreeSortGUI extends JFrame {


	// The panels and the sub panels to make the correct layout
	private JPanel mainPanel, panelOGList, panelSortList, 
	panelPerformSort,panelContain, 
	panelSortOrder, panelNumType, panelExit;

	//Declare Labels
	private JLabel originalList;
	private JLabel sortedList;

	//Declare Textfields
	private JTextField originalListTextField;
	private JTextField sortedListTextField;

	// Declare Buttons
	private JButton pSortButton;
	private JButton exitButton;

	// JRadioButtons
	private JRadioButton ascendingJButton;
	private JRadioButton descendingJButton;
	private JRadioButton integerJButton;
	private JRadioButton fractionJButton;

	// ButtonGroup for the JRadioButtons
	private ButtonGroup sortGroup;
	private ButtonGroup numGroup;

	// Border for the JRadioButtons
	TitledBorder sortBorder;
	TitledBorder numBorder;

	/**
	 * Default constructor
	 */
	public BinarySearchTreeSortGUI() {

		instantiate();

	} 

	/*
	 * Constructor that passes in our Tree so we may use it for our GUI.
	 * @param <T>
	 * @param ExpTree1 - ExpressionTreeEvaluator that I created that will
	 * do our work.
	 * This is where GUI is build along with buttons, listeners
	 *  size, and other specifications.
	 * 
	 */


	/**
	 * Constructor that passes in Binary Search Tree so I can use it in the GUI
	 * @param <T> BST1 Generic Type of Binary Search Tree, class that I created 
	 * and will carry out the work with the defined methods
	 * This is where GUI is built along with buttons, JradioButtons, Borders, 
	 * size, layout and other specifications. This is also where GUI
	 * interacts with userInput and worker classes and its methods
	 */
	public <T> BinarySearchTreeSortGUI(BinarySearchTree<T> BST1) {

		// Call methods that will generate GUI
		instantiate();
		buildGUI();
		add(mainPanel);
		addListeners();

		// Set Title
		setTitle("Binary Search Tree Sort");

		// Set default jframe close operation
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Set Size
		setSize(700,450);

		// Lock Size of window
		setResizable(false);

		// Set window visible
		setVisible(true);

		pack();

	}

	/**
	 * Add the listners to my JButtons (Perform Sort, CLose) and 
	 * JRadioButtons(ascending/descending, integer/fraction)
	 */
	private void addListeners() {
		// TODO Auto-generated method stub


		/*
		 * Perform Sort
		 * 
		 * Ascending/Descending
		 * 
		 * Integer/Fraction
		 */



		/*
		 * Perform Sort Button
		 */

		/**
		 * Give sort button action Listener (what it does, and catch exception)
		 */
		pSortButton.addActionListener(new ActionListener() {

			/**
			 * Required method w/ ActionListener to any buttons
			 */
			public void actionPerformed(ActionEvent e) {



				/*
				 * - Get the list from userInput (TextField)
				 * - Set the sorted list to blank for now
				 * - Declare Generic Tree that I will create by iterating through
				 */
				String data = originalListTextField.getText();

				sortedListTextField.setText("");

				BinarySearchTree<?> BSTFromData = null;


				/*
				 * Verify data entered, and they selected a sort option and numeric type
				 * for the JRadioButtons
				 * Once I verified user entered data with
				 * sorting and numeric options make a Tree with the type according
				 * to what the user picked (Integer ||or Fraction)
				 */
				if (data.equals("")) 
				{
					JOptionPane.showMessageDialog(null, "Enter data to sort");
					return;
				}

				if (!ascendingJButton.isSelected() && 
						!descendingJButton.isSelected()) 
				{
					JOptionPane.showMessageDialog(null, "Select an option for Sort Order");
					return;
				}

				if (!integerJButton.isSelected() && 
						!fractionJButton.isSelected()) 
				{
					JOptionPane.showMessageDialog(null, "Select the data type of data");
					return;
				}

				// Make a Integer BST
				if (integerJButton.isSelected()) 
				{
					BSTFromData= new BinarySearchTree<Integer>();

				}			else	{ 			// Make a Fraction BST

					BSTFromData = new BinarySearchTree<Fraction>();
				}




				String values[] = data.split(" ");


				try {

					/*
					 * Make an array to put our data in it, iterate through
					 * it and make the Tree accordingly 
					 */
					//Integer
					if (integerJButton.isSelected()) 
					{
						Integer alist[] = new Integer[values.length];

						//iterate
						for (int i = 0; i < values.length; i++) 
						{
							alist[i] = Integer.valueOf(values[i]);
						}

						BinarySearchTree<Integer> root = 
								((BinarySearchTree<Integer>)BSTFromData)
								.convertArrayToBST(alist, alist.length);

						// Make an empty tree so it can be filled
						BSTFromData.textlist = "";

						if (ascendingJButton.isSelected())
						{
							BSTFromData.ascending(root);

						}			else	{

							BSTFromData.descending(root);
						}

						sortedListTextField.setText(BSTFromData.textlist);
					}

					//Else Fraction
					else

					{

						Fraction alist[] = new Fraction[values.length];

						for (int i = 0; i < values.length; i++) 
						{
							if(values[i].split("/").length > 2)
							{
								throw new NumberFormatExpression();
							}

							if(!values[i].contains("/"))
							{
								Integer.parseInt(values[i]);
								values[i] = values[i]+"/1";
							}

							Fraction fract = new Fraction(values[i]);

							alist[i] =fract;
						}

						BinarySearchTree<Fraction> root = ((BinarySearchTree<Fraction>)
								BSTFromData).convertArrayToBST(alist, alist.length);

						BSTFromData.textlist = "";

						if (ascendingJButton.isSelected())
						{
							BSTFromData.ascending(root);
						}

						else	
						{
							BSTFromData.descending(root);
						}
						sortedListTextField.setText(BSTFromData.textlist);

					}

				} //END OF TRY TIME TO CATCH POSSIBLE ERRORS


				//catch NumberFormatExpression Exception
				catch (NumberFormatExpression error) {

					JOptionPane.showMessageDialog(null, error.getErrorMessage());
					return;
				}

				catch (Exception invalidChar) {

					JOptionPane.showMessageDialog(null, "Non Numerical Input, Try Again");
					return;
				}

			}
		});


		/**
		 * Add Action Listener for exit Button.
		 */
		exitButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				//close the program
				System.exit(0);

			}
		});


	}

	/**
	 * Instantiates the GUI components:
	 *  mainPanel and corresponding subPanels, JLabels
	 *  JTextFields, JButtons, JRadioButtons, Groups and Borders
	 *  to match the assignments description
	 */
	private void instantiate() {
		// Initialize GUI elements

		// Initialize Panels
		mainPanel = new JPanel(); 
		panelOGList = new JPanel(); 
		panelSortList = new JPanel();
		panelPerformSort = new JPanel();
		panelContain = new JPanel();
		panelSortOrder = new JPanel();
		panelNumType = new JPanel();
		panelExit = new JPanel();

		// Initialize Labels
		originalList = new JLabel("Original List");
		sortedList = new JLabel("Sorted List");

		// Initialize Fields
		originalListTextField = new JTextField(25);
		sortedListTextField = new JTextField(25);

		// Set display fields to be not editable for result
		sortedListTextField.setEditable(false);

		// Initialize Buttons
		pSortButton = new JButton("Perform Sort");
		exitButton = new JButton("Exit");

		// Init JRadioButtons
		ascendingJButton = new JRadioButton("Ascending");
		descendingJButton = new JRadioButton("Descending");
		integerJButton = new JRadioButton("Integer");
		fractionJButton = new JRadioButton("Fraction");

		// Init ButtonGroup
		sortGroup = new ButtonGroup();
		numGroup = new ButtonGroup();

		// Init Border for JRadionButtons
		sortBorder = BorderFactory.createTitledBorder("Sort Order");
		numBorder = BorderFactory.createTitledBorder("Numeric Type");

		// make Borders Sort Order and Numeric sType
		panelSortOrder.setBorder(sortBorder);
		panelNumType.setBorder(numBorder);

	}

	/**
	 * Builds the GUI by adding the components to the frame. 
	 * This is setting the layouts/looks and adding our 
	 * subPanels to where it is added accordingly in our main panel.
	 * Also where I add all JRadioButtons, Buttons, TextField and Labels
	 */
	private void buildGUI() {

		// Set Layout
		mainPanel.setLayout(new GridLayout(5, 2));

		panelOGList.setLayout(new FlowLayout());
		panelSortList.setLayout(new FlowLayout());
		panelPerformSort.setLayout(new FlowLayout());

		panelContain.setLayout(new GridLayout(1,1));

		panelSortOrder.setLayout(new GridLayout(2, 1));
		panelNumType.setLayout(new GridLayout(2, 1));

		panelExit.setLayout(new FlowLayout());

		// add JRadioButtons to group
		sortGroup.add(ascendingJButton);
		sortGroup.add(descendingJButton);

		numGroup.add(integerJButton);
		numGroup.add(fractionJButton);

		// Add Fields-Labels-Buttons to sub Panels
		panelOGList.add(originalList); 
		panelOGList.add(originalListTextField); 

		panelSortList.add(sortedList);
		panelSortList.add(sortedListTextField);

		panelPerformSort.add(pSortButton);

		panelSortOrder.add(ascendingJButton);
		panelSortOrder.add(descendingJButton);

		panelNumType.add(integerJButton);
		panelNumType.add(fractionJButton);

		panelContain.add(panelSortOrder);
		panelContain.add(panelNumType);

		panelExit.add(exitButton);

		// Add subPanels to main content panel
		mainPanel.add(panelOGList);
		mainPanel.add(panelSortList);
		mainPanel.add(panelPerformSort);
		mainPanel.add(panelContain);
		mainPanel.add(panelExit);		

	}







	/**
	 * Main Function where I will initialize a new BinarySearchTreeSortGUI and 
	 * pass in a generic Binary Search Tree.
	 * @param args
	 */
	public static <T> void main(String[] args) {

		BinarySearchTree<T> BSTTest = new BinarySearchTree<T>();


		new BinarySearchTreeSortGUI(BSTTest);
	}
}







