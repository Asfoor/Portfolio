package appointmentBook;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Abstract Appointment class, depending on type of appointment, will incorporate different occur_on method, other classes will use its toString and getInfo
 * @author Anes Mehai
 *
 */
public abstract class Appointment {

	private char type; 
	private GregorianCalendar calendar;
	private String description; 

	/**
	 * Constructor
	 * @param calendar - Gregorian calendar object
	 * @param description - description of the appointment
	 */
	public Appointment(GregorianCalendar calendar, String description){

		this.calendar = calendar;
		this.description = description;

	}

	/**
	 * Get the type of appointment
	 * @return type - type of appointment (O,D,W)
	 */
	public char getType() {
		return type;
	}
/**
 * Set the type of appointment
 * @param type - type of appointment (O,D,W)
 */
	public void setType(char type) {
		this.type = type;
	}

	/**
	 * Get the calendar (day, month, year, hour, minute)
	 * @return = calendar, GregorianCalendar object
	 */
	public GregorianCalendar getCalendar() {
		return calendar;
	}

	/**
	 * Set the calendar (day, month, year, hour, minute)
	 * @param calendar
	 */
	public void setCalendar(GregorianCalendar calendar) {
		this.calendar = calendar;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	abstract boolean occurs_on(GregorianCalendar calendar);


	public String getInfo()
	{
		String message = "";

		Date now = calendar.getTime();

		DateFormat dateDisplay = DateFormat.getTimeInstance(DateFormat.SHORT);
		
		message = dateDisplay.format(now) + " " + getDescription() + "\n";

		return message;
	}

	public String toString()
	{
		String message = "";

		Date now = calendar.getTime();

		DateFormat dateDisplay = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT);
		
		message = dateDisplay.format(now) + " " + getDescription();

		return message;
	}
}

