package appointmentBook;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 * 
 * @author Anes Mehai
 * Appointments class that implements AppointmentsInterface. The class is responsible for adding and display appointment
 *
 */
public class Appointments implements AppointmentsInterface {


	ArrayList<Appointment> listAppt = new ArrayList<Appointment>();

	/**
	 * Add the appointments to the ArrayList of Appointment object
	 * 
	 * @param type - A char, Type of appointment (Once, Daily or Weekly)
	 * @param date - A GregorianCalendar object
	 * @param description - A String of the description of the appointment
	 */
	@Override
	public void addAppt(char type, GregorianCalendar date, String description) {
		// TODO Auto-generated method stub


		switch (type) {

		case 'O': listAppt.add(new OneTime(date, description));
		break;

		case 'W': listAppt.add(new Weekly(date, description));
		break;

		case 'D': listAppt.add(new Daily(date, description));
		break;

		}
	}
	/**
	 * Display the appointments and return the value as a String. Later the GUI will take the string and display to the user.
	 * 
	 * @param day - an int that is responsible for the day of the appointment
	 * @param month - an int that is responsible for the month of the appointment
	 * @param year - an int that is responsible for the year of the appointment
	 */
	@Override
	public String displayAppt(int day, int month, int year) {

		String value = "";
		String message = "Appointments on " + (month + 1)+ "/" + day + "/"+ year + "\n\n";


		for (int i = 0; i < listAppt.size(); i++)
		{
			Appointment appointment = listAppt.get(i);

			GregorianCalendar calendar = new GregorianCalendar(year, month, day);


			if(appointment.occurs_on(calendar))
			{
				value += listAppt.get(i).getInfo();
			}



		}

		if (value.equals(""))
		{
			value = "There are no appointments on " +(month + 1) + "/" + day  + "/" + year;
		}

		else
		{
			value = message + value;
		}

		return value;
	}

	/**
	 * @return count - amount of appointments
	 */
	@Override
	public int GetAppointmentCount() {

		int count = listAppt.size();

		return count;
	}


	/**
	 * Read the file from the JFile Chooser and then take what is read from the file and add the appointments to the ArrayList Appointment
	 * 
	 * @param fileRead - The scanner will read this.
	 */
	public void readFile(File fileRead) {

		Scanner in = null;
		try {
			in = new Scanner(fileRead);
			while (in.hasNextLine()) {

				char type = in.next().charAt(0);
				int month = in.nextInt();
				int day = in.nextInt();
				int year = in.nextInt();
				int hour = in.nextInt();
				int minute = in.nextInt();
				String description = in.nextLine();

				GregorianCalendar date = new GregorianCalendar(year, month, day, hour, minute);

				addAppt(type, date,description );
			}
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		finally {
			in.close();
		}

	}


	/**
	 * @return message - Information inside the ArrayList
	 */
	public String toString(){
		String message = "";

		for(int i = 0; listAppt.size() > i; i++ )
		{
			message += listAppt.get(i).toString() + "\n";
		}
		return message;
	}

}
