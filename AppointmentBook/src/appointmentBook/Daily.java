package appointmentBook;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * 
 * @author Anes Mehai
 * 
 * Daily Class that extends Appointment, in charge of Daily occurrences of an Appointment 
 *
 */
public class Daily extends Appointment {

/**
 * Constructor
 * @param calendar - Takes in a GregorianCalendar object name calendar
 * @param description - Takes in a String that describes the Appointment
 */
	public Daily(GregorianCalendar calendar, String description) {
		super(calendar, description);

	}
/**
 * Will make sure that Daily occurences happen everyday
 */
@Override
boolean occurs_on(GregorianCalendar calendar) {
	
	boolean occur = true;
	
	if (getCalendar().get(Calendar.YEAR) > calendar.get(Calendar.YEAR))
	{
		occur = false;
	}
	
	return occur;
}


	

}
