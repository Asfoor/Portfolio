package appointmentBook;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * 
 * @author Anes Mehai
 * 
 * Weekly Class that extends Appointment, in charge of weekly occurrences of an Appointment 
 *
 */
public class Weekly extends Appointment {


	/**
	 * Constructor
	 * @param calendar - Takes in a GregorianCalendar object name calendar
	 * @param description - Takes in a String that describes the Appointment
	 */
	public Weekly(GregorianCalendar calendar, String description) {
		super(calendar, description);

	}

	/**
	 * Will make sure that weekly occurences happen once a week
	 */
	@Override
	boolean occurs_on(GregorianCalendar calendar) {

		boolean occur = false;



		if(getCalendar().get(Calendar.DAY_OF_WEEK) == calendar.get(Calendar.DAY_OF_WEEK))
		{
			occur = true;
		}

		if (getCalendar().get(Calendar.YEAR) > calendar.get(Calendar.YEAR))
		{
			occur = false;
		}


		return occur;


	}

}
