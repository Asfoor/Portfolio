package appointmentBook;
import static org.junit.Assert.*;

import java.util.GregorianCalendar;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;


public class AnesTest {

	
	Appointments apt1;
	
	
	@Before
	public void setUp() throws Exception {
		
		apt1 = new Appointments();
		apt1.addAppt('O', new GregorianCalendar(2015, 0, 05,10, 0), "Dr. Appt");
		apt1.addAppt('W', new GregorianCalendar(2015, 0, 04, 9, 30), "Zumba Class");
		apt1.addAppt('D', new GregorianCalendar(2015, 0, 01, 8, 0), "Walk the Dog");
		apt1.addAppt('O', new GregorianCalendar(2015, 0, 21, 20, 30), "Eric's Bday Party");
		apt1.addAppt('D', new GregorianCalendar(2015, 0, 15, 7, 30), "Treadmill");
		apt1.addAppt('W', new GregorianCalendar(2015, 1, 1, 18, 30), "Call Mom");
		apt1.addAppt('W', new GregorianCalendar(2015, 1, 2, 16, 0), "Band Practice");
		apt1.addAppt('W', new GregorianCalendar(2015, 0, 27, 15, 30), "Piano Lessons");
		apt1.addAppt('O', new GregorianCalendar(2015, 1, 14, 19, 0), "Valentine Dinner");
	}

	@After
	public void tearDown() throws Exception {
		
		apt1 = null;
	}
	

	@Test
	public void test() {
		fail("Not yet implemented");
	}

}
