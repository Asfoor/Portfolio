package appointmentBook;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import java.awt.BorderLayout;
import java.io.File;
import java.util.GregorianCalendar;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;




/**
 * The GUI aspect of the project, will communicate with the Driver class to carry task Will add appointments, display appointments to screen based on date given and read file to take in appointments.
 * 
 * @author Anes Mehai
 */
public class AppointmentBookGui extends JFrame {

	// Constants for the window size
	private final int WINDOW_WIDTH = 800;
	private final int WINDOW_HEIGHT = 800;

	// The panels and the sub panels
	private JPanel mainPanel, north, center, subCenter, subCenter2,
	south, subSouth, subSouth2, subSouth3;

	// TextFields
	private JTextField dayEntry;
	private JTextField monthEntry;
	private JTextField yearEntry;
	private JTextField hourEntry;
	private JTextField minuteEntry;
	private JTextField descriptionEntry;

	// Labels
	private JLabel day;
	private JLabel month;
	private JLabel year;
	private JLabel hour;
	private JLabel minute;
	private JLabel apptDescription;

	// JRadioButtons
	private JRadioButton radio1; // one time
	private JRadioButton radio2; // weekly
	private JRadioButton radio3; // daily

	// ButtonGroup
	private ButtonGroup group;

	// JButtons
	private JButton addAppt;
	private JButton displayAppt;
	private JButton readFile;
	private JButton exit;

	// Border
	private TitledBorder Border;
	private TitledBorder Border2;
	private TitledBorder Border3;

	// Appointments Objects
	Appointments appt1;

	// JFileChooser
	private JFileChooser fileChooser = new JFileChooser();

	// File
	private File selectedFile;

	/**
	 * Constructor to make panels and GUI.
	 */
	public AppointmentBookGui() {

		appt1 = new Appointments();

		// Set the window's title
		setTitle("Appointment Book");

		// Specify what happens when the close button is pressed
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Make the GUI a FlowLayout
		setLayout(new BorderLayout());

		// Build the panel that contains the other components
		buildPanel();
		buildNorthPanel();
		buildCenterPanel();
		buildSouthPanel();

		// Add the panel to the content pane
		add(mainPanel);
		add(north, BorderLayout.NORTH);
		add(center, BorderLayout.CENTER);
		add(south, BorderLayout.SOUTH);

		// Set the window's size
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);

		// Lock Size of window
		setResizable(false);

		// Display the window
		setVisible(true);

		// Pack everything
		pack();
	}

	/**
	 * Makes the panel that all the other subPanels will go into
	 */
	private void buildPanel() {
		// make mainPanel with BorderLayout
		mainPanel = new JPanel();
	}

	/**
	 * Builds the north part of the panel. A radio group with buttons one-time, weekly and daily.
	 */
	private void buildNorthPanel() {

		// make Panel with Border
		north = new JPanel();
		Border = BorderFactory.createTitledBorder("Appointment Type");
		north.setBorder(Border);

		// make ButtonGroup
		group = new ButtonGroup();

		// make buttons
		radio1 = new JRadioButton("One Time");
		radio2 = new JRadioButton("Weekly");
		radio3 = new JRadioButton("Daily");

		// add JRadioButtons to group
		group.add(radio1);
		group.add(radio2);
		group.add(radio3);

		// add radio buttons to north Panel
		north.add(radio1);
		north.add(radio2);
		north.add(radio3);
	}

	/**
	 * Builds the center part of the Panel. Border layout for Appointment Date and Time and TextFields and labels for Day Month Year Hour and Minute
	 */
	private void buildCenterPanel(){

		// make Panel with GridLayout
		center = new JPanel();
		center.setLayout(new GridLayout(1, 2));

		// make 2 sub Panels
		subCenter = new JPanel();
		subCenter.setLayout(new GridLayout(3,3));

		subCenter2 = new JPanel();
		subCenter2.setLayout(new GridLayout(3,3));

		// make Labels
		day = new JLabel("Day");
		month = new JLabel("Month");
		year = new JLabel("Year");

		hour = new JLabel("Hour");
		minute = new JLabel("Minute");

		// make Textfields
		dayEntry = new JTextField(5);
		monthEntry = new JTextField(5);
		yearEntry = new JTextField(5);

		hourEntry = new JTextField(5);
		minuteEntry = new JTextField(5);

		// Appointment Date
		Border2 = BorderFactory.createTitledBorder("Appointment Date");
		subCenter.setBorder(Border2);

		subCenter.add(day);
		subCenter.add(dayEntry);
		subCenter.add(month);
		subCenter.add(monthEntry);
		subCenter.add(year);
		subCenter.add(yearEntry);

		// Appointment Time
		Border3 = BorderFactory.createTitledBorder("Appointment Time");
		subCenter2.setBorder(Border3);

		subCenter2.add(hour);
		subCenter2.add(hourEntry);
		subCenter2.add(minute);
		subCenter2.add(minuteEntry);

		// add subPanels to center Panel
		center.add(subCenter);
		center.add(subCenter2);
	}

	/**
	 * Builds the south part of the panel. Appt Description label and textfield, and 4 buttons (Add Appt, Display Appt, Read File and Exit)
	 */
	private void buildSouthPanel(){

		// make Panel with GridLayout
		south = new JPanel();
		south.setLayout(new GridLayout(3, 1));


		// make Labels
		apptDescription = new JLabel("Appt Description");

		// make Textfields
		descriptionEntry = new JTextField(10);

		//make buttons
		addAppt = new JButton("Add Appt");
		displayAppt = new JButton("Display Appts");
		readFile = new JButton("Read File");
		exit = new JButton("Exit");

		// make 3 sub Panels
		subSouth = new JPanel();
		subSouth.setLayout(new GridLayout(2,1));

		subSouth2 = new JPanel();
		subSouth2.setLayout(new FlowLayout());

		subSouth3 = new JPanel();

		// subSouth
		subSouth.add(apptDescription);
		subSouth.add(descriptionEntry);

		// subSouth2
		subSouth2.add(addAppt);
		subSouth2.add(displayAppt);
		subSouth2.add(readFile);

		//subSouth3
		subSouth3.add(exit);

		// add subPanels to south Panel
		south.add(subSouth);
		south.add(subSouth2);
		south.add(subSouth3);

		// add ActionListeners to the buttons
		addAppt.addActionListener(new buttonListener());
		displayAppt.addActionListener(new buttonListener());
		readFile.addActionListener(new buttonListener());
		exit.addActionListener(new buttonListener());

	}

	// buttonListener class that implements ActionListener
	// In here buttons and what they do will be defined
	/**
	 * 
	 * buttonListener class that implements an ActionListener. This class will
	 * handle the events of what happens when a button is clicked
	 * 
	 */
	private class buttonListener implements ActionListener {

		/**
		 * Clears all the textFields
		 */
		public void clear() {

			// Make all fields empty by putting space in it
			dayEntry.setText("");
			monthEntry.setText("");
			yearEntry.setText("");
			hourEntry.setText("");
			minuteEntry.setText("");
			descriptionEntry.setText("");
		}

		/**
		 * Every ActionListener needs this method. This method will define what
		 * each of the buttons does when it is pressed. Returns nothing and
		 * makes sure none of the value entered is negative
		 * 
		 * @param e
		 *            An ActionEvent object. With an ActionEvent we will be able
		 *            to know what the user pressed. Depending on what the user
		 *            clicks
		 */
		@Override
		public void actionPerformed(ActionEvent e) {

			// Gets whatever button was pressed
			String buttonText = ((JButton) e.getSource()).getText();

			// To compare buttonText to the button pressed
			String button1 = "Add Appt";
			String button2 = "Display Appts";
			String button3 = "Read File";
			String button4 = "Exit";

			// To get entries from GUI and convert it for the Gregorian Calendar
			String dayText = dayEntry.getText();
			String monthText = monthEntry.getText();
			String yearText = yearEntry.getText();
			String hourText = hourEntry.getText();
			String minuteText = minuteEntry.getText();
			String descriptionText = descriptionEntry.getText();

			// for type in constructor
			char type = 'O';



			// if statements to determine the type based on radio button selected
			if (radio1.isSelected())
			{
				type = 'O';

			}

			if (radio2.isSelected())
			{
				type = 'W';

			}

			if (radio3.isSelected())
			{
				type = 'D';

			}

			if (buttonText.equals(button1)) // Add Appt
			{
				GregorianCalendar date = new GregorianCalendar(Integer.parseInt(yearText),Integer.parseInt(monthText),
						Integer.parseInt(dayText),Integer.parseInt(hourText),Integer.parseInt(minuteText));

				String messageMonth = validateMonth(Integer.parseInt(monthText));

				if ( messageMonth != null )
				{
					JOptionPane.showMessageDialog(null, messageMonth, "Invalid User Input", JOptionPane.ERROR_MESSAGE);	
				}

				else{

					String messageDay = validateDay(Integer.parseInt(dayText), Integer.parseInt(monthText));

					if (messageDay != null )
					{
						JOptionPane.showMessageDialog(null, messageDay, "Invalid User Input", JOptionPane.ERROR_MESSAGE);
					}
					else
					{
						String messageYear = validateYear(Integer.parseInt(yearText));
						if (messageYear != null)
						{
							JOptionPane.showMessageDialog(null, messageYear, "Invalid User Input", JOptionPane.ERROR_MESSAGE);	
						}

						else{

							String messageHours = validateHours(Integer.parseInt(hourText));

							if (messageHours != null)
							{
								JOptionPane.showMessageDialog(null, messageHours, "Invalid User Input", JOptionPane.ERROR_MESSAGE);	
							}

							else{

								String messageMinutes = validateMinutes(Integer.parseInt(minuteText));

								if (messageMinutes != null)
								{
									JOptionPane.showMessageDialog(null, messageMinutes, "Invalid User Input", JOptionPane.ERROR_MESSAGE);	
								}

								else
								{
									appt1.addAppt(type, date, descriptionText);
									clear();
								}
							}


						}

					}

				}


			}

			if (buttonText.equals(button2)) // Display Appts
			{

				String display = appt1.displayAppt(Integer.parseInt(dayText), (Integer.parseInt(monthText) -1 ), Integer.parseInt(yearText));




				String messageMonth = validateMonth(Integer.parseInt(monthText));

				if ( messageMonth != null )
				{
					JOptionPane.showMessageDialog(null, messageMonth, "Invalid User Input", JOptionPane.ERROR_MESSAGE);	
				}

				else {

					String messageDay = validateDay(Integer.parseInt(dayText), Integer.parseInt(monthText));

					if ( messageDay != null)
					{
						JOptionPane.showMessageDialog(null, messageDay, "Invalid User Input", JOptionPane.ERROR_MESSAGE);	
					}

					else{

						String messageYear = validateYear(Integer.parseInt(yearText));
						if (messageYear != null)
						{
							JOptionPane.showMessageDialog(null, messageYear, "Invalid User Input", JOptionPane.ERROR_MESSAGE);	
						}

						else{

							JOptionPane.showMessageDialog(null, display);
						}
					}
				}
			}
			if (buttonText.equals(button3)) // Read File

			{

				// save this code for whenever you want to read a File in GUI
				int returnVal = fileChooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					selectedFile = fileChooser.getSelectedFile();
					String filename = selectedFile.getPath();
					JOptionPane.showMessageDialog(null, "You selected "
							+ filename);

					appt1.readFile(new File(filename));
				}
			}

			if (buttonText.equals(button4)) // Exit
			{
				setVisible(false);
				System.exit(0);
			}

		}

	}

	/**
	 * main, Create AppointmentBookGui contructor to run GUI
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		new AppointmentBookGui();

	}

	/**
	 * Validate that the month passed in is a month that is between 1 - 12 (January - December)
	 * @param month - month (1 - 12)
	 * @return error - returns nothing or an String error message
	 */
	private String validateMonth(int month)
	{
		String error = null;

		if(month > 12 || month < 0)
		{
			error =  "Invalid entry. There are 12 months in a year. Please re-enter valid entry (0 - 12).";
		}

		return error;
	}

	/**
	 * Validate that the date falls inside the month passed in
	 * @param day - day depending on the month between (1 - n) where n is the last day for that corresponding month
	 * @param month - month (1 - 12)
	 * @return error - return nothing or an String error message
	 */
	private String validateDay(int day, int month)
	{
		String error = null;

		switch(month)
		{

		case 1: 
		{
			if(day < 1 || day > 31)
			{
				error = "Days can only be 1 - 31 for January";
			}
		}
		break;

		case 2: 
		{
			if(day < 1 || day > 28)
			{
				error = "Days can only be 1 - 28 for February";
			}
		}
		break;

		case 3: 
		{
			if(day < 1 || day > 31)
			{
				error = "Days can only be 1 - 31 for March";
			}
		}
		break;

		case 4: 
		{
			if(day < 1 || day > 30)
			{
				error = "Days can only be 1 - 30 for April";
			}
		}
		break;

		case 5: 
		{
			if(day < 1 || day > 31)
			{
				error = "Days can only be 1 - 31 for May";
			}
		}
		break;

		case 6: 
		{
			if(day < 1 || day > 30)
			{
				error = "Days can only be 1 - 30 for June";
			}
		}
		break;

		case 7: 
		{
			if(day < 1 || day > 31)
			{
				error = "Days can only be 1 - 31 for July";
			}
		}
		break;

		case 8: 
		{
			if(day < 1 || day > 31)
			{
				error = "Days can only be 1 - 31 for August";
			}
		}
		break;

		case 9: 
		{
			if(day < 1 || day > 30)
			{
				error = "Days can only be 1 - 30 for September";
			}
		}
		break;

		case 10: 
		{
			if(day < 1 || day > 31)
			{
				error = "Days can only be 1 - 31 for October";
			}
		}
		break;

		case 11: 
		{
			if(day < 1 || day > 30)
			{
				error = "Days can only be 1 - 30 for November";
			}
		}
		break;

		case 12: 
		{
			if(day < 1 || day > 31)
			{
				error = "Days can only be 1 - 31 for December";
			}
		}
		break;

		}

		return error;

	}

	/**
	 * Validate that the minutes passed in is between 0 - 59 minutes
	 * @param minutes - minutes in an hour (0 - 59)
	 * @return error - returns nothing or an String error message
	 */
	private String validateMinutes(int minutes)
	{
		String error = null;

		if( minutes > 59 || minutes < 0)
		{
			error = "There are 59 minutes in an hour. Please re-enter valid entry (0 - 59)";
		}
		return error;

	}

	/**
	 * Validate that the hours passed in is between 0 - 24 hours
	 * @param hours - hours in an day (0 - 24)
	 * @return error - returns nothing or an String error message
	 */
	private String validateHours(int hours)
	{
		String error = null;

		if (hours > 24 || hours < 0)
		{
			error = "There are 24 hours in a day. Please re-enter valid entry (0 - 24)";
		}
		return error;

	}

	/**
	 * Validate that the years passed in is a valid year (4+ digits)
	 * @param years - years passed is modern (1000+)
	 * @return error - returns nothing or an String error message
	 */
	private String validateYear(int year)
	{
		String error = null;

		if (year < 1000)
		{
			error = "Please enter a valid year with 4 digits";
		}
		return error;

	}

}

