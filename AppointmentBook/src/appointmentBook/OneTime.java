package appointmentBook;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * OneTime appointments that extend Appointment, will occur one time
 * @author Anes Mehai
 *
 */
public class OneTime extends Appointment {


	/**
	 * Constructor
	 * @param calendar - Gregorian calendar object
	 * @param description - description of appointment
	 */
	public OneTime(GregorianCalendar calendar, String description) {
		super(calendar, description);
	}

	/**
	 * Will make sure that OneTime occurence happen one time
	 */
	@Override
	boolean occurs_on(GregorianCalendar calendar) {

		boolean occur = false;

		if(	getCalendar().get(Calendar.YEAR) == calendar.get(Calendar.YEAR) && 
				getCalendar().get(Calendar.MONTH) == calendar.get(Calendar.MONTH) &&
				getCalendar().get(Calendar.DATE) == calendar.get(Calendar.DATE))
		{
			occur = true;
		}

		return occur;
	}

}
