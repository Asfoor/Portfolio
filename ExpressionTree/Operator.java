package ExpressionTree;

/**
 * 
 * Operator is a class that extends the parent class Node.
 * 
 * Operator is used in the algorithm to build the Tree.
 * The code has a default constructor, a constructor that takes in the
 * data of the current node and creates a leaf node.
 * 
 * There are the methods getData, getLeftside, getRightside, 
 * setLeftside, setRightside & toString. It uses the super class (Node) to get 
 * corresponding value.
 * 
 *
 * @author Anes Mehai
 * 
 */
public class Operator extends Node {


	/**
	 * Default Constructor that calls the node constructor using super();
	 */
	public Operator() {

		super();
	}

	/**
	 * Operator constructor that takes the data from the node and creates object
	 * with data, leftside and rightside nodes set to null.
	 * @param data, Current value of current node
	 * @param right, Pointer of next element
	 * @param left, Pointer of previous element
	 */
	public Operator(char data, Node left, Node right) {

		super.setData(data);

		super.setLeftside(null);

		super.setRightside(null);

	}

	/**
	 * Get the data/value of the current node
	 */
	public char getData() {

		char message = super.getData();

		return message;
	}

	/**
	 * Get the previous Node
	 * @return messageL, the leftside Node
	 */
	public Node getLeftside() { 

		Node messageL = super.getLeftside();

		return messageL;
	}

	/**
	 * Get the next Node
	 * @return messageR, the rightside Node
	 */
	public Node getRightside() {  

		Node messageR = super.getRightside();

		return messageR;
	}

	/**
	 * Set the leftside for the new node
	 * @param leftside node
	 */
	public void setLeft(Node leftside) {

		super.setLeftside(leftside);

	}

	/**
	 * Set the rightside for the new node
	 * @param rightside node
	 */
	public void setRight(Node rightside) {

		super.setRightside(rightside);
	}

	/**
	 * ToString method that will be used for 3 address generator
	 */
	public String toString()
	{

		/*
		 * We use this toString to manipulate the message we want with R1 and all that
		 */
		// Use ro r1 r3 and divide if/switch statement
		String sMessage = "( "+super.getLeftside()+" "+super.getData()+" "+super.getRightside()+" )"; 

		System.out.println(sMessage);

		return sMessage;
	}
}

