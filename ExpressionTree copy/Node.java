package ExpressionTree;



/*
 * Inheritance allows a class to use the properties and methods of another class. 
 * In other words, the derived class inherits the states and behaviors from the base class. 
 * The derived class is also called subclass and the base class is also known as super-class. 
 * The derived class can add its own additional variables and methods. 
 * These additional variable and methods differentiates the derived class from the base class.
 */

/**
 * 
 * Abstract class Node, this is the parent class, each child will have access to
 * data of the current node and the next and previous node instructions. 
 * 
 * @author Anes Mehai
 *
 */
public abstract class Node {

	/*
	 * Set abstract methods to be implemented
	 * Initiliaze node, class that extends node will have to declare these
	 * 3 data variables.
	 */


	//This is the data/value of current node/position
	private char data;

	//pointers to next node
	//Our left and right #s
	//To be used using setters and getters .super()
	private Node leftside; //previous head.
	private Node rightside; //next tail

	/**
	 * Default Constructor
	 */
	public Node () {

	}

	/**
	 * 
	 * 
	 * @param data, The number of the current node
	 * @param leftside, The instructions on where the previous node is
	 * @param rightside, The instructions on where the next node is
	 */
	public Node (char data, Node leftside, Node rightside) {


	}


	/**
	 * Set the value for data
	 * @param data, data of our current Node, position.
	 */
	public void setData(char data) {

		this.data = data;
	}

	/**
	 * Getter for data
	 * @return data (value) of current character
	 */
	public char getData() {

		return data;
	}


	/**
	 * 
	 * @return
	 */
	public Node getLeftside() {

		return leftside;
	}


	/**
	 * 
	 * @param leftside
	 */
	public void setLeftside(Node leftside) {

		this.leftside = leftside;
	}


	/**
	 * 
	 * @return
	 */
	public Node getRightside() {

		return rightside;
	}


	/**
	 * 
	 * @param rightside
	 */
	public void setRightside(Node rightside) {

		this.rightside = rightside;
	}


	/**
	 * 
	 */
	public String toString() {

		String message = getData() + "";

		return message;

	}




}

