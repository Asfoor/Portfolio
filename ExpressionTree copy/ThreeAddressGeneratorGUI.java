package ExpressionTree;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * This class is our GUI, it is where our main is and where we run our program.
 * This class extends JFrame and uses Swing to provide our GUI. Every instance variable
 * must be declared as private.
 * 
 * This class works by having the constructor call 4 methods:
 * 
 *  - instantiate my variables, buttons, labels, etc so I can use them
 *  - buildGui: builds my gui with the layout and look I want
 *  - and adding my mainPanel made of subPanels and Listeners for my buttons.
 * 
 *
 * 
 * Running this program will bring up a GUI where the user will enter a postfix
 * expression. From there they will construct a tree and from that tree it will
 * return the expression in infix notation. Lastly it will generate a file with
 * the 3 address format instructions.
 * 
 * To run our program we must create our ThreeAddressGeneratorGUI and pass in our
 * TreeExpressionEvaluator to build our Tree, and do the work to convert the postfix
 * expression to infix expression. 
 * 
 * Project 2:
 * 
 * - A program that accepts an arithmetic expression of unsigned integers in postfix 
 * notation
 * - And builds the arithmetic expression tree that represents that expression.
 * 
 * - From that tree, the corresponding fully parenthesized infix expression 
 * should be displayed
 * - And a file should be generated that contains the 3 address format instructions. 
 *
 *
 *
 * @author Anes Mehai
 */
public class ThreeAddressGeneratorGUI extends JFrame {


	// The panels and the sub panels to make the correct layout
	private JPanel mainPanel, panelOne, panelTwo, panelThree, panelFour;

	//Declare Labels
	private JLabel postfixExpression;
	private JLabel infixExpression;

	//Declare Textfields
	private JTextField postfixField;
	private JTextField infixField;

	// Declare Buttons
	private JButton constructTree;
	private JButton exitButton;


	/**
	 * Default constructor
	 */
	public ThreeAddressGeneratorGUI() {

		instantiate();

	}

	/**
	 * Constructor that passes in our Tree so we may use it for our GUI.
	 * @param ExpTree1 - ExpressionTreeEvaluator that I created that will
	 * do our work.
	 * This is where GUI is build along with buttons, listeners
	 *  size, and other specifications.
	 * 
	 */
	public ThreeAddressGeneratorGUI(ExpressionTreeEvaluator ExpTree1) {

		// Call methods that will generate GUI
		instantiate();
		buildGUI();
		add(mainPanel);
		addListeners();

		// Set Title
		setTitle("Three Address Generator");

		// Set default jframe close operation
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Set Size
		setSize(500,200);

		// Lock Size of window
		setResizable(false);

		// Set window visible
		setVisible(true);

		pack();

	}

	/**
	 * Instantiates the GUI components:
	 *  mainPanel and corresponding subPanels, JLabels
	 *  JTextFields, and JButtons to match the assignments description
	 */
	private void instantiate() {

		// Initialize GUI elements

		// Initialize Panels
		mainPanel = new JPanel(); 
		panelOne = new JPanel(); 
		panelTwo = new JPanel();
		panelThree = new JPanel();
		panelFour = new JPanel();

		// Initialize Labels
		postfixExpression = new JLabel("Enter Postfix Expression");
		infixExpression = new JLabel("Infix Expression");


		// Initialize Fields
		postfixField = new JTextField(20);
		infixField = new JTextField(16);

		// Set display fields to be not editable for result
		infixField.setEditable(false);


		// Initialize Buttons
		constructTree = new JButton("Construct Tree");
		exitButton = new JButton("Exit");

	}

	/**
	 * Builds the GUI by adding the components to the frame. 
	 * This is setting the layouts/looks and adding our 
	 * subPanels to where it is added accordingly in our main panel.
	 * 
	 */
	private void buildGUI() {

		// Set Layout
		mainPanel.setLayout(new GridLayout(4, 1));
		panelOne.setLayout(new FlowLayout());
		panelTwo.setLayout(new FlowLayout());
		panelThree.setLayout(new FlowLayout());


		// Add Fields-Labels-Buttons to sub Panels
		panelOne.add(postfixExpression); 
		panelOne.add(postfixField);
		panelTwo.add(constructTree);
		panelThree.add(infixExpression);
		panelThree.add(infixField);
		panelFour.add(exitButton);

		// Add subPanels to main content panel
		mainPanel.add(panelOne);
		mainPanel.add(panelTwo);
		mainPanel.add(panelThree);
		mainPanel.add(panelFour);

	}

	/**
	 * Adds listeners and actions to the GUI buttons: contruct tree and quit
	 */
	private void addListeners() {

		// Add listener to button
		/**
		 * Add an action Listener to our Construct button,
		 * Gets the postfix expressions builds Tree and
		 * outputs infix expression in the infixfield.
		 */
		constructTree.addActionListener(new ActionListener() {

			/**
			 * The calculation that goes on from deriving the Tree
			 * @param e ActionEvent
			 * @throws RuntimeException, Check for invalid tokens
			 */
			public void postfixToInfixCalculate (ActionEvent e) throws RuntimeException{


				String input = postfixField.getText();

				ExpressionTreeEvaluator ee = null;

				try {

					ee = new ExpressionTreeEvaluator(input);

				} catch (Exception error) {

					// TODO Auto-generated catch block

					JOptionPane.showMessageDialog(null,error.getMessage(),"Mesage",1);				}

					infixField.setText(ee.toString());

			}
			
			/**
			 * Give action to the button. 
			 * Postfix ton Infix notation conversion.
			 */
			public void actionPerformed(ActionEvent e) {

				postfixToInfixCalculate(e);

			}

		});

		// Add listener to exit button
		/**
		 * Listen to Button
		 */
		exitButton.addActionListener(new ActionListener() {

			/**
			 * Give Button action
			 */
			public void actionPerformed(ActionEvent e) {

				//close the program
				System.exit(0);
			}
		});
	}

	/**
	 * @param args 
	 * Main where we Declare our Tree and GUI objects
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ExpressionTreeEvaluator et1 = new ExpressionTreeEvaluator();

		// Create instance of ThreeAddressGenerator GUI
		new ThreeAddressGeneratorGUI(et1);

	}

}
