package ExpressionTree;

/*
 * Make Javadoc
 * Change class name
 * Our ExpressionTree, working class. 
 */


/**
 *
 *
 *	This is where we take the user inputed expression, add 
 *
 *
 * @author 
 */
public class ExpressionTreeEvaluator { //Name ExpressionTreeEvaluator

	/*
	 * Initiliaze all variables that I will use
	 */

	// Flag that checks if our node is empty
	boolean addedCheck = false;

	Node root;






	/**
	 * Default Constructor
	 */
	public ExpressionTreeEvaluator() {
		
	}

	/**
	 * Contructor
	 * 
	 * @param data
	 * @throws Exception
	 */
	public ExpressionTreeEvaluator(String data) throws Exception {


		for(int i=data.length()-1;i>=0;i--)
		{
			addedCheck=false;

			root=addNode(root, data.charAt(i));
		}
	}



	private Node addNode(Node node,char data) throws RuntimeException
	{
		if(node!=null)
		{
			if(node instanceof Operator)
			{
				if(node.getRightside() == null||node.getRightside() instanceof Operator)
				{
					node.setRightside(addNode(node.getRightside(), data));
				}

				else if(!addedCheck && node.getRightside() !=null &&
						node.getRightside() instanceof Operand)
				{
					if(node.getLeftside() == null || node.getLeftside() instanceof Operand)
					{
						node.setLeftside(addNode(node.getLeftside(), data));
					}
				}

				if(!addedCheck && node.getLeftside() == null
						||node.getLeftside() instanceof Operator)
				{
					node.setLeftside(addNode(node.getLeftside(), data));
				}

			}

		}
		else
		{
			addedCheck=true;

			if(isOperator(data))
			{

				return new Operator(data, node, node);
			}

			else
			{
				return new Operand(data, node, node);
			}
		}

		return node;

	}

	/**
	 * Checks if the next element
	 * @param error
	 * @return
	 * @throws Exception
	 */
	private boolean isOperator(char error) throws RuntimeException{

		if(error == '+' || error == '-' || error == '*' || error =='/' || error == '^')
		{
			return true;
		}

		if(error >= '0'&& error <='9')
		{

			return false;
		}

		throw new RuntimeException("Invalid token, * Please enter expression in"
				+ "correct notation with valid tokens * " + error);


	}



	@Override
	public String toString()
	{
		return root.toString();
	}


}

