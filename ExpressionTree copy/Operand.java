package ExpressionTree;
/**
 * Operand is a class that extends the parent class Node.
 * 
 * Operand are the numbers getting worked on.
 * Operand is used in the algorithm to build the Tree.
 * The code has a default constructor, a constructor that takes in the
 * data of the current node and creates a leaf node.
 * 
 * There are the methods toString which getsData and converts it to a String.
 * All the operands in the expression are returned for toString method
 * 
 * @author Anes Mehai
 */
public class Operand extends Node {

	/**
	 * Default Constructor
	 */
	public Operand() {

		super();
	}

	/**
	 * Operand constructor that takes the data from the node and creates object
	 * with data, leftside and rightside nodes set to null.
	 * @param data, Current value of current node
	 * @param right, Pointer of next element
	 * @param left, Pointer of previous element
	 */
	public Operand(char data, Node left, Node right) {

		super.setData(data);

		super.setLeftside(null);

		super.setRightside(null);

	}

	/**
	 * Override toString to get the message we want
	 * This is all the operand in the expression
	 */
	public String toString()
	{

		String dMessage = super.getData() + "";

		return dMessage;

	}
}
