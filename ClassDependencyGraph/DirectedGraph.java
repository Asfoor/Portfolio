package ClassDependencyGraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Stack;
import java.util.Map.Entry;

import ClassDependencyGraph.CycleDetectedException;
import ClassDependencyGraph.InvalidClassNameException;
import ClassDependencyGraph.Vertex;

/**
 * Worker Class:
 * This is where Graph is built, the GUI will use this class object
 * to carry out the functionality of the program.
 *
 * This class is generic allowing for a generic type for the vertex names.
 * In this application those names are Strings. 
 * 
 * The graph is represented as an arrayList of vertices that contain
 * A LinkedList of their associated adjacency lists(child)
 * 
 * The adjacency lists should be lists of integers that represent the index
 * rather than vertex name itself.
 * 
 * A HashMap is used to associate vertex names with their index in the list of vertices
 * 
 * @author Anes Mehai
 *
 * @param <T>
 */
public class DirectedGraph <T> {

	/*
	 * - The graph should be represented as an array list of vertices that 
	 * contain a linked list of their associated adjacency lists.
	 * - The adjacency lists should be lists of integers that represent the index
	 * rather than the vertex name itself/
	 * - A hash map should be used to associate vertex names with their index in the 
	 * list of vertices
	 */

	//Declaration and initialization of LinkList, HashMap and index, have everything private


	/*
	 * HashMap<K,V>
	 * K - the type of keys maintained by this map, vertex names
	 * V - the type of mapped values, index in the list of vertices
	 * Using HashMap allows the execution time of basic operations such as get()
	 * and put() to remain constant even for large sets
	 */
	private HashMap<String, Vertex> vertexMap = new HashMap<>();

	private HashMap<Integer, String> vertices;




	private ArrayList<LinkedList<Integer>> adjacencies;
	private ArrayList<String> headList;

	//Hold a Stack of Vertex, if vertex is found push that vertex onto the stack
	private Stack<Vertex> vertStack = new Stack();

	//Place counter
	private int currentIndex = 0;


	/**
	 * Default Constructor, Basic Init
	 */
	public DirectedGraph() {

		vertices = new HashMap<>();
		vertexMap = new HashMap<>();

		adjacencies = new ArrayList<>();
		headList = new ArrayList<>();

	}

	/**
	 * Method 1
	 * Initializes the graph and builds the adjacency list from user Inputed File
	 * @param input- Scanner, Scan file, iterate, store data and derive Graph from it
	 */
	public void initGraph(Scanner inputData) {

		Scanner currentLine;

		String currentName;

		Vertex<String> currentVertex;  //Bunch of Vertex represented by Strings

		//Traditional method of reading data from stream using Scanner class
		while (inputData.hasNextLine()) { //while more lines keeping running

			//Save data from input Stream
			currentLine = new Scanner(inputData.nextLine());

			currentName = currentLine.next(); 

			headList.add(currentName); //Add current String vertex into headOfList

			//Looks at HashMap and if Key has currentName
			if (vertexMap.containsKey(currentName)) 
			{

				/*
				 * V get(Object key)
				 * This method returns the value to which the specified key is mapped, 
				 * or null if this map contains no mapping for the key.
				 */ //Get first, get key Values
				currentVertex = vertexMap.get(currentName);


				/*
				 * If Current Vertex I'm at reappears in the current 
				 * Line then throw Cycle Exception
				 * If s is discovered throw cycle detected exception
				 */
				if (currentVertex.isDiscovered()) 
				{
					throw new CycleDetectedException(currentVertex.getValue());
				}
				else 	{ //otherwise move on, mark s as discovered

					currentVertex.setDiscovered();
				}
			}

			//Build Graph accordingly
			else	 {

				/*
				 * V put(K key, V value)
				 * This method associates the specified value with the specified key 
				 * in this map.*/  //Then push new Vertex with Hashcode value
				vertexMap.put(currentName, new Vertex<String>(currentName));
			}

			//new elements
			while(currentLine.hasNext()) {

				String newName = currentLine.next();

				if (vertexMap.containsKey(newName)) 
				{
					Vertex<String> newVertex = vertexMap.get(newName);

					//Add new element, child from vertex. 
					// Connections of parent vertex and child vertex is an edge
					vertexMap.get(currentName).addChild(newVertex);

					//If s is discovered throw cycle detected exception
					if (newVertex.isDiscovered()) 
					{
						throw new CycleDetectedException(newVertex.getValue());
					}

					else 	{ //mark as discovered

						newVertex.setDiscovered();
					}
				}

				else 	{

					//HashMap functions

					vertexMap.put(newName, new Vertex<String>(newName));

					vertexMap.get(currentName).addChild(vertexMap.get(newName));
				}
			}
		}

		int vertexIndex = 0;
		String newName = "";

		/*
		 * Iterate though headList and find the root(head)
		 */
		for (String head : headList) 
		{

			//Go to root Node and buildAdjacency from it, will not be in order
			//but still efficient b/c of HashMap

			currentVertex = vertexMap.get(head);

			buildAdjacencyList(currentVertex);
		}

	}

	/**
	 *  Take the vertex and add to it its adjacency list (classes that depend on it) that
	 *  vertex and all of its children, using the addEdge method
	 * @param vertex
	 */
	public void buildAdjacencyList(Vertex<String> vertex) {

		String currentName = "";
		int vertexIndex = 0;
		String newName = "";

		currentName = vertex.getValue(); //make currentName equal vertex's value

		//Gather info to build adjacency/dependency list

		//Hashmap has name of currentNode I'm on
		if (vertices.containsValue(currentName)) 
		{
			vertexIndex = getKey(currentName);
		}

		else 	{ //if not add it in

			vertexIndex = currentIndex;
			vertices.put(currentIndex, currentName); //put away

			currentIndex++; //update place on index & add new LinkedList with associated adjacency list 
			adjacencies.add(new LinkedList<Integer>());
		}

		//If the vertex has dependencies then call addEdge and buildAdjacency
		if (vertex.hasChildren())
		{
			//iterate through vertex of string and get the child of each parent vertices
			//then addEdges from parent to adjacency & its child
			for (Vertex<String> child : vertex.getChildren()) 
			{
				addEdge(vertex, child); 
				buildAdjacencyList(child);
			}
		}
	}

	/**
	 * Method 2
	 * This will add an edge to the corresponding adjacency list
	 * Linking source/parent and destination/child/adjacency
	 * 
	 * v-w
	 * @param source(v)
	 * @param destination(w)
	 */
	public void addEdge(Vertex<String> source, Vertex<String> destination) {

		//every edge has a start and end point (source & destination)
		int sourceIndex = getKey(source.getValue());
		int destinationIndex;


		if (vertices.containsValue(destination.getValue())) 
		{
			//have destinationIndex be the key of destination.value
			destinationIndex = getKey(destination.getValue());
		}

		else	 { //add destination of edge from source.

			destinationIndex = currentIndex;
			currentIndex++;

			vertices.put(destinationIndex, destination.getValue()); //put
			adjacencies.add(new LinkedList<Integer>());
		}

		adjacencies.get(sourceIndex).add(destinationIndex); //get

	}

	/**
	 * Method 3
	 * 
	 * Topological Button that returns a String representation of the topological order
	 * of the graph from the input class name.
	 * 
	 * Throws a DNE exception if Classname is not valid or not String
	 * 
	 * Uses Depth_first_search to carry out task
	 * 
	 * @param vertexName, the type of key.
	 * @return answer, String representation of topological order
	 */
	public String topologicalOrder(String vertexName) {

		/* Resets the discovered and finished flags for each vertex
		 * map entry (key-value pair). 
		 * 
		 * The Map.entrySet method returns a collection-view of 
		 * the map, whose elements are of this class. 
		 * The only way to obtain a reference to a map entry is from the iterator 
		 * of this collection-view. These Map.Entry objects are valid only for the 
		 * duration of the iteration; more formally, the behavior of a map entry is undefined 
		 * if the backing map has been modified after the entry was returned by the iterator, 
		 * except through the setValue operation on the map entry.*/

		for (Entry<String, Vertex> entry : vertexMap.entrySet()) 
		{
			entry.getValue().reset();
		}

		//Declare empty answer String
		String answer = "";

		if (vertices.containsValue(vertexName)){

			depthFirstSearch(vertexMap.get(vertexName));

			while (!vertStack.isEmpty()) {
				answer += vertStack.pop().getValue() + " ";
			}

			return answer;
		}

		else { //invalid ClassName

			throw new InvalidClassNameException(vertexName);
		}
	}


	/**
	 * Helper method 
	 * Simplifies getting the corresponding key for given value (currentName) in the vertices
	 * HashMap
	 * @param value, the type of mapped values
	 * @return key for mapped values, or -1 (false) if not found
	 */
	public Integer getKey(String value) {

		//iterate through
		for(Entry<Integer, String> entry : vertices.entrySet()) 
		{
			if (entry.getValue().equals(value)) 
			{
				return entry.getKey();
			}
		}

		return -1;
	}


	/**
	 * Helper method needed for Topological Order, check pseudocode
	 * 
	 * Depth First Search algorithm method to generate Topological Order
	 * when given vertex. This will run when Topological Order is pressed
	 * 
	 * @param vertex, Vertex from File
	 */
	public void depthFirstSearch(Vertex<String> vertex) {

		//From provided pseudocode


		if (vertex.isDiscovered()) //if vertex discovered throw cycle exception
		{ 

			throw new CycleDetectedException(vertex.getValue());
		}

		//if s is finished return
		if (vertex.isFinished()) 
		{

			return;
		}

		//mark s vertex as discovered
		vertex.setDiscovered();

		//for all adjacent vertices v
		if (vertex.hasChildren()) 
		{
			//depth_first_search(v)
			for (Vertex<String> child : vertex.getChildren()) 
			{
				depthFirstSearch(child);
			}

		}

		//mark s as finished & push onto the stack
		vertex.setFinished();

		vertStack.push(vertex);

	}

}