package ClassDependencyGraph;

/**
 *Implements an Exception for when there is a cycle detected within
 * the user Input file Graph
 * @author Anes Mehai
 *
 */
public class CycleDetectedException extends RuntimeException {

	//String representation of the Vertex involved in a Cycle
	private String vertexCycle = "";

	/**
	 * Default Constructor
	 */
	public CycleDetectedException() {
	}

	/**
	 * Constructor that will take the vertex that is causing Error and Display Error Message
	 * @param vertexName
	 */
	public CycleDetectedException(String vertexName) {
		vertexCycle = vertexName;
	}

	/**
	 * Getter- Get what's inside vertexCycle
	 * @return vertexCycle - Vertex that is involved in a cycle (repeated path)
	 */
	public String getVertexCycle() {
		return vertexCycle;
	}
}
