package ClassDependencyGraph;


/**
 * Exception that is thrown when the user inputs the name of a class
 * not found in the graph, fileName selected.
 * @author Anes Mehai
 *
 */
public class InvalidClassNameException extends RuntimeException {

	// Which class is illegal and causing an error which we have to handle
	private String illegalClass = "";

	/**
	 * Default Constructor
	 */
	public InvalidClassNameException() {
	}

	/**
	 * 
	 * @param className, the user input class that is not valid. 
	 * Inside main GUI, this is thrown and caught when filling in
	 * class to recompile.and pressing topological order
	 */
	public InvalidClassNameException(String className) {

		illegalClass = className;
	}

	/**
	 * Getter - Get the name of the class that is causing an error,
	 * in GUI this will be handled with throw and catch clauses
	 * @return illegalClass - class that user input that is invalid
	 */
	public String getClassName() {

		return illegalClass;
	}
}
