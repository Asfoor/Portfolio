package ClassDependencyGraph;

import java.util.*;

/**
 *  Implements the methods and fields needed to create a class for
 *  a vertex in a directed graph
 *  
 *  Every Vertex has:
 *  1 Name
 *  2 Value
 *  3 Data containing associated adjacency/child 
 *  
 * @author Anes Mehai
 *
 * @param <T> Generic Type Name, Vertex, String etc
 */
public class Vertex<T> {

	/*
	 * Instance data that all Vertex have.
	 * Need them to carry out Topological Order and add from Graph to Array Structures
	 */
	private ArrayList<Vertex<T>> children;

	//Generic value
	private T value;

	//For DFSearch must have discovered() and finished() functions
	private boolean discovered = false;
	private boolean finished = false;

	/**
	 * Default Constructor
	 */
	public Vertex(){

	}

	/**
	 * Constructor that take in Generic value
	 * @param value
	 */
	public Vertex(T value) {
		this.value = value;
		children = new ArrayList<>();
	}

	/**
	 * Constructor that takes in Generic Value and ArrayList of child Vertices
	 * @param value
	 * @param children
	 */
	public Vertex(T value, ArrayList<Vertex<T>> children) {
		this.value = value;
		this.children = children;
	}

	/**
	 * Add the child Vertex
	 * @param child
	 */
	public void addChild(Vertex child) {
		children.add(child);
	}

	/**
	 * Get the child vertex
	 * @return children element that we are looking for
	 */
	public ArrayList<Vertex<T>> getChildren() {
		return children;
	}

	/**
	 * Get generic value of whatever type passed
	 * @return value, Generic Type
	 */
	public T getValue() {
		return value;
	}

	/**
	 * Returns true or false if current element has children
	 * @return true or false
	 */
	public boolean hasChildren() {

		boolean yesOrNo = children.size() > 0;

		return yesOrNo;
	}

	/**
	 * Returns true or false if this element has already been passed/discovered
	 * @return discovered, true or false
	 */
	public boolean isDiscovered() {
		return discovered;
	}

	/**
	 * Returns true or false if current element has been processed and is Finished/loggedin
	 * @return finished
	 */
	public boolean isFinished() {
		return finished;
	}

	/**
	 * Set discovered to true
	 */
	public void setDiscovered() {
		discovered = true;
	}

	/**
	 * Set finished to true
	 */
	public void setFinished() {
		finished = true;
	}

	/**
	 * Rsets the discovered and finished flags/boolean
	 */
	public void reset() {
		
		discovered = false;
		finished = false;
	}

}
