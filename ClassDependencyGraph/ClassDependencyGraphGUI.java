package ClassDependencyGraph;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import ClassDependencyGraph.CycleDetectedException;
import ClassDependencyGraph.InvalidClassNameException;

/**
 * March 4, 2017
 * 
 * This class is our GUI, it is where our main is and where we run our program.
 * This class extends JFrame and uses Swing to provide our GUI. Every instance variable
 * must be declared as private.
 * 
 * This class works by having the constructor call 4 methods:
 * 
 *  - instantiate my variables, buttons, labels, TextArea, Borders, etc so I can use them
 *  - buildGui: builds my gui with the layout and look I want
 *  - and adding my mainPanel made of subPanels 
 *  - and Listeners for my buttons.
 * 
 * 
 * 		Running this program will bring up a GUI where the user enter a file name
 * to be read. It will generate a Graph from the data it read in.
 * Once the graph is built it will say successful, or did not open.
 * 
 * If opened time in a class to recompile in the given textfield to 
 * print the topological order of the graph in the TextArea.
 *  
 * To run our program we must create our ClassDependencyGraphGUI and pass in our
 * DirectedGraph to build our Graph, and do the work to give the topological order.
 * 
 * Project 4:
 * 
 * The project involves writing a program that behaves like the Java command line compiler.
 * 
 * - Pressing the Build Directed Graph button should cause the specifiec input file
 * that contains the class dependency info to be read in and the directed graph
 * represented by those dependencies to be built.
 * - After pressing the Build Directed Graph button, one of the 2 following messages
 * will be generated:
 * 	1	Graph Built Successfully
 * 	2	File Did Not Open
 * - Once the graph has been buil the name of a class to be recompiled can be 
 * specified and the Topological Order button can be pressed.
 * 
 * - When circular dependencies exist run CycleDetectedException Error
 * 
 * - If no cycle is detected in the TextArea will print the topological order
 * for the specified class Name
 *
 */

public class ClassDependencyGraphGUI extends JFrame {

	DirectedGraph Graph; //worker class object so we can use methods
	boolean initialized = false; //If True pass=graph build. If false fail=no graph

	// The panels and the sub panels to make the correct layout
	private JPanel mainPanel, panelFileName, panelRecompile, 
	subPanel1,subPanel2,subPanel3, 
	panelRecompileOrder;

	//Declare Labels
	private JLabel fileName;
	private JLabel recompile;

	//Declare Textfields
	private JTextField graphTextField;
	private JTextField classTextField;

	// Declare Buttons
	private JButton buildGraph;
	private JButton topologicalOrder;

	//Declare JTextArea
	private JTextArea recompOrder;

	// Border for the JTextArea
	TitledBorder orderBorder;

	/**
	 * Default constructor, Basic Init
	 */
	public ClassDependencyGraphGUI() {

		instantiate();
	} 

	/**
	 * Constructor that passes in DirectedGraph so it can use it in the GUI
	 * @param <T> Generic Type of Directed Graph, take any string data 
	 * of elements inside Directed Graph 
	 * @param graph1, Pass in Graph (DirectedGraph) worked class, for GUI to use.
	 * Directed Graph class will carry out the work with the defined methods.
	 * This is where GUI is built along with buttons, TextArea, Borders, 
	 * size, layout and other specifications. This is also where GUI
	 * interacts with userInput and worker classes and its methods
	 */
	public <T> ClassDependencyGraphGUI(DirectedGraph<T> graph1) {

		// Call methods that will generate GUI
		instantiate();
		buildGUI();
		add(mainPanel);
		addListeners();

		// Set Title
		setTitle("Class Dependency Graph");

		// Set default jframe close operation
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Set Size
		setSize(1000,700);

		// Set window visible
		setVisible(true);

		pack();
	}


	/**
	 * Instantiates the GUI components:
	 *  mainPanel and corresponding subPanels, JLabels
	 *  JTextFields, JButtons, JTextArea and Borders
	 *  to match the assignments description
	 */
	private void instantiate() {
		// Initialize GUI elements


		//Initialize JTextArea
		recompOrder = new JTextArea();

		//In order to ensure everything gets read to consul for large data
		recompOrder.setWrapStyleWord(true);

		// Initialize Panels
		mainPanel = new JPanel(); 
		panelFileName = new JPanel(); 
		panelRecompile = new JPanel();
		subPanel1 = new JPanel();
		subPanel2 = new JPanel();
		subPanel3 = new JPanel();
		panelRecompileOrder = new JPanel();

		// Initialize Labels
		fileName = new JLabel("Input File Name:");
		recompile = new JLabel("Class To Recompile:");

		// Initialize Fields
		graphTextField = new JTextField(15);
		classTextField = new JTextField(15);

		recompOrder = new JTextArea();
		recompOrder.setEditable(false);

		// Initialize Buttons
		buildGraph = new JButton("Build Directed Graph");
		topologicalOrder = new JButton("Topological Order");

		// Init Border for Recompilation Order Area
		orderBorder = BorderFactory.createTitledBorder("Recompilation Order");

		// make Borders for JText Area
		panelRecompileOrder.setBorder(orderBorder);		
	}

	/**
	 * Builds the GUI by adding the components to the frame. 
	 * This is setting the layouts/looks and adding our 
	 * subPanels to where it is added accordingly in our main panel.
	 * Also where I add all JRadioButtons, Buttons, TextField and Labels
	 */
	private void buildGUI() {

		// Set Layout
		mainPanel.setLayout(new GridLayout(2, 1));

		panelFileName.setLayout(new FlowLayout());
		panelRecompile.setLayout(new FlowLayout());

		panelRecompileOrder.setLayout(new BorderLayout());

		subPanel1.setLayout(new GridLayout(2,3));
		subPanel2.setLayout(new GridLayout(1,1));
		subPanel3.setLayout(new GridLayout(1,1));


		// Add Fields-Labels-Buttons, (graph and class) to sub panels

		//FileName panel and so on..
		panelFileName.add(fileName); 
		panelFileName.add(graphTextField); 
		panelFileName.add(buildGraph); 

		panelRecompile.add(recompile);
		panelRecompile.add(classTextField);
		panelRecompile.add(topologicalOrder); 

		//add this to sub panel1
		subPanel1.add(panelFileName);
		subPanel1.add(panelRecompile);

		panelRecompileOrder.add(recompOrder);

		//add this to sub panel2
		subPanel2.add(panelRecompileOrder);

		// Add subPanels to main content panel
		mainPanel.add(subPanel1);
		mainPanel.add(subPanel2);
	}


	/**
	 * Add the listeners to my JButtons Build Directed Graph & Topological Order  
	 */
	private void addListeners() {

		//Listeners for BuildGraph and TopologicalOrder

		/*
		 * Build Graph from user input fileName
		 * buildGraph Button
		 */

		/*
		 * Give Build Directed Graph button action Listener 
		 * (what it does) and catch exception.
		 */
		buildGraph.addActionListener(new ActionListener() {

			/**
			 * Method needed to give action to Build Graph Button
			 */
			public void actionPerformed(ActionEvent event) {

				/*Make Scanner and retrieve fileName from input
				 * save the name and use it to scan the File, .... then 
				 * make new Graph and init.
				 */

				Scanner scan = null;

				String fileName = graphTextField.getText();

				//Surround with 2 exceptions have to catch/
				//Read in file using scanner to init and make graph
				try {

					/*
					 * BufferedReader better than other way 
					 * because take the need to iterate (while loop), using delimeters
					 * and easier to implement
					 */
					scan = new Scanner(new BufferedReader(new FileReader(fileName))); //BufferedRader take in fileReader and reads fileName

					Graph = new DirectedGraph(); 

					Graph.initGraph(scan);

					//Flag that confirms Graph has been initiliazed
					initialized = true;



					//Success JOptionPane
					JOptionPane.showMessageDialog
					(null,"Graph built succesfully!", "Message",1);
				}



				//end of try for build Graph, now catch possible exceptions

				catch (IOException e) { //FileName DNE, error1

					JOptionPane.showMessageDialog
					(null,"File Did Not Open!", "Message",1);

				}//end of catch for DNE


				catch (CycleDetectedException e) { //CycleFound Not a Graph, error2

					JOptionPane.showMessageDialog(null, "A Cycle Has Been Detected Please Try A "
							+ "Different File Input With No Cycle (Repetition of Nodes.)", 
							"Cycle Detected", JOptionPane.ERROR_MESSAGE);

				}//end of catch for Cycle

				//end of exceptions

				finally { //make sure to always close scanner, will run no matter what


					if (scan != null)  //if Scanner is empty
					{
						scan.close(); //then close Scanner :)

					}
				}

			}

		}); // end of Build Graph =====================================================

		/*
		 * From graph recompile class given from user Input
		 * and place answer in JTextArea
		 * topologicalOrder Button
		 */

		/*
		 * Give Topological Order button action Listener 
		 * (what it does) and catch exception. InvalidClassName.
		 */
		topologicalOrder.addActionListener(new ActionListener() {


			/**
			 * Method to give action to Topological Order button
			 */
			public void actionPerformed(ActionEvent event) {

				//If initialized is False! No graph
				if (!initialized)
				{
					JOptionPane.showMessageDialog(null, "No graph has been initialized. "
							+ "Please initialize a graph.","Invalid Graph", 
							JOptionPane.ERROR_MESSAGE);
				}


				// Graph has been initialized and created successfully.
				else {

					//Put recompilationOrder in the TextArea and check for exceptions
					try {

						recompOrder.setText(Graph.topologicalOrder(classTextField.getText()));

					} //end of try for topological order


					// Invalid class name exception
					catch (InvalidClassNameException classException) {
						JOptionPane.showMessageDialog(null, classException.getClassName() +
								" Is Not A Valid Class Name Friend. " + 
								"Please Enter A Valid Class Name.", "Invalid Class Name",
								JOptionPane.ERROR_MESSAGE);

					} 	//end of catch
				}

			}

		}); // end of topological order

	} // end of addListeners


	/**
	 * Main where I create GUI and DirectedGraph and run it.
	 * @param args
	 */
	public static <T> void main(String[] args) {

		//create Generic graph graph1 and pass it in to GUI
		DirectedGraph<T> graph1 = new DirectedGraph<T>();

		new ClassDependencyGraphGUI(graph1);
	}
}